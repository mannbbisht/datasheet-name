select
--ROUND(0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))/2.16 + ((60-[Compressed air temperature above cooling medium temperature C])*(576.38*1.2/60)) /(1.2*[Cooling air flow m s]),1)
[Datasivun numero]
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1) AS 'SHAFT POWER'
--,convert(float,replace([Volumetric efficiency],',','') )
,Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2) as 'capacity'
,Round(((((60-[Compressed air temperature above cooling medium temperature C]) *(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*(convert(float,replace([Volumetric efficiency],',','') )/100	)/1000)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2)) *1.2))
/60) +
(0.04*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2)*1.1))/60*2260*9,0)
,round((((isnull([Fan motor kW EW],0)+ (Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))/1000)*1.07)*1000000/(SQRT(3)* [1 Voltage] * [Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current for package440'
,((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) *(100-[Motor efficiency]/100)) +(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02)

  from data
	   WHERE (CODE ='TMC54-85' AND [Capacity at normal working pressure m min] IS   NULL AND([Main motor temperature rise] IS NULL OR [Main motor temperature rise] IS NOT NULL))
	AND [Datasivun numero]='MDATA0622' and Laskuri=3207