USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData]    Script Date: 4/5/2021 11:18:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData]
AS
SELECT Laskuri, [Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +'  V / ' +  [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +'  V / ' +  [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage',[Normal working pressure barg] as 'Normal working pressure barg',
[Capacity at normal working pressure m min] * 60 as 'Capacity at normal working pressure m min calculated' ,[Capacity at normal working pressure m min] as 'Capacity at normal working pressure m min'
,[Shaft power at normal working pressure kW] as 'Shaft power at normal working pressure kW'
,Cast([Maximum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Maximum working pressure barg]/10 as nvarchar(20)) as 'Maximum working pressure barg(mPag)'
,Cast([Minimum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Minimum working pressure barg]/10 as nvarchar(20)) as 'Minimum working pressure barg(mPag)'
,ROUND([Shaft power at normal working pressure kW]*0.22,1) as 'Shaft power at normal working pressure kW calculated'
,Transmission  as 'Tranmission', [Speed of rotation rpm],
case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Transmissioncalculated',
[Motor pulley],[Air end pulley],
case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus',cast('0' as nvarchar(20))+cast(' - ' as nvarchar(12)) +cast([Maximum ambient temperature C] as nvarchar(12)) as 'Allowed ambient temperature MIN MAX C'
,cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12)) as 'Compressed air temperature above cooling medium temperature C'
,[Cooling air flow m s] as 'Cooling air flow m s',[Maximum cooling air pressure drop Pa] as 'Maximum cooling air pressure drop Pa'
,[Water in C Fresh water cooled] as 'Water in C Fresh water cooled'
,[Water out C Fresh water cooled] as 'Water out C Fresh water cooled'
,[Water in C Sea water cooled] as 'Water in C Sea water cooled'
,[Water out C Sea water cooled] as 'Water  Out C Sea water cooled'
,cast([Minimum water inlet pressure with zero ba bar MPa Parallel] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa Serial]/10) as nvarchar(12)) as 'Minimum water inlet pressure with zero ba bar MPa Parallel' 
,[Minimum water inlet pressure with zero back bar MPa After] as 'Minimum water inlet pressure with zero back bar MPa After'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water in out T2 T3'
,[Main motor F class IP 55 kW] as 'Main motor F class IP 55 kW'
,cast([Fan motor kW EA] as nvarchar(max)) + ' / ' +  cast(isnull([Fan motor kW EW],'') as nvarchar(max))  as 'Fan motor kW EA / EW'
,[Speed of rotation rpm fan motor] as 'Speed of rotation rpm fan motor'
,[Fuse max 1 J�nnite A]
,[Fuse max 2 J�nnite A]
,[Motor cos fii]
,[Motor efficiency]
,[Starting current Ia In DOL YD SOFT START]
,[Control voltage V]
,[Oil quantity I]
,[Oil content mg m]
,[Oil quantity EA I]
,[Air outlet T1]
,[Condensate drain T4]
,[Pressure Level LpA with NOVOX canopy EA]
,[Pressure Level LpA with NOVOX canopy EW]
,[Pressure Level LpA without canopy EA]
,[Pressure Level LpA without canopy EW]
,[Weight without canopy]
,[Weight with NOVOX canopy]
,[Weight with NOVOX canopy EA]
,[General arrangement drawing EA]
,[General arrangement drawing FW]
,[General arrangement drawing SW]
,[General arrangement drawing with NOVOX canopy EA]
,[General arrangement drawing with NOVOX canopy EW]
,[Cable gland Power]
,[Cable gland Alarm Signal]
,Lis�tietoja as 'Extra notes'
,[Datasheet revision]
,[Maximum working pressure barg] as 'Maximum working pressure barg single row'
,cast(round(([Shaft power at normal working pressure kW] *0.95),1) as nvarchar(12)) + ' / '+ CAST(ROUND(([Shaft power at normal working pressure kW]) * 0.08,1) AS NVARCHAR(12)) as 'Heat dissipation'
,[Maximum ambient temperature C] AS 'HW_MaximumamidentTemperature'
,[Shaft power at normal working pressure kW]*0.7 AS 'HYD_SHAFTPOWER'
,case when Transmission ='B'THEN (CEILING(([Hydraulic motor speed of rotation Max] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Hydraulic motor speed of rotation Max]
WHEN Transmission='G' THEN (CEILING([Hydraulic motor speed of rotation Max] * [Transmission Gear i]))
ELSE '' END AS 'HYDTransmissioncalculated'
, CASE WHEN CAST([Compressed air temperature above cooling medium temperature C] AS NVARCHAR(12)) > 6 THEN '6 - ' + CAST([Compressed air temperature above cooling medium temperature C] AS NVARCHAR(12)) END AS 'HYD_COMPRESSEDAIRTEMPERATURE'
, CASE WHEN CAST([Water in C Fresh water cooled] AS NVARCHAR(12)) > 0 then  CAST([Water in C Fresh water cooled] AS NVARCHAR(12)) +' / ' + CAST([Water out C Fresh water cooled] AS NVARCHAR(12)) else CAST([Water in C Fresh water cooled] AS NVARCHAR(12)) end 'HYD_WATERINCFRESHWATERCOOLED'
 ,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(12)) +' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial] /10 AS NVARCHAR(12)) AS 'HYD_INLETPRESSUREZEROBACK'

 ,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(12)) +' / ' + CAST([Minimum water inlet pressure with zero back Serial sea] /10 AS NVARCHAR(12)) AS 'HYD_inlet_pressure_with_zero_back_Serial_sea'
 ---Minimum water inlet pressure with zero back (Serial)_sea
 ,[Shaft power at normal working pressure kW]*0.75 AS 'HYD_OILCOOLERREJECTED'
 ,[Shaft power at normal working pressure kW] *0.08 AS 'HYD_HEATDISSIPATION'
 ,[Hydraulic main motor] AS 'HYD_MAINMOTOR'
 ,CAST([Hydraulic motor speed of rotation Max] AS NVARCHAR(12)) +' / ' + CAST( [Hydraulic motor speed of rotation Min] AS NVARCHAR(12)) AS 'Hydraulic_motor_speed_rotation_max_min'
 ,CAST([Hydraulic oil flow l min] AS NVARCHAR(12)) + ' / '+ CAST([Hydraulic oil flow l min min] AS NVARCHAR(12)) AS 'Hydraulic_oil_max_min'
 ,CAST([Hydraulic oil pressure bar] AS nvarchar(12)) +' / '+ CAST( [Hydraulic oil pressure bar min] AS NVARCHAR(12)) AS'Hydraulic_oil_pressure_bar_max_min'
 ,[Air outlet T1 water cooled]
 ,[2 Voltage]
 ,(Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       (4.2 *( [water out c fresh water cooled] - [water in c fresh water cooled] ) ) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) 
       , 2))  AS 'HYD_Water_flow_Fresh_water_cooled_60_45' 
	   ,(Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       (4.2 *( [Water out C Sea water cooled] - [Water in C Sea water cooled] ) ) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) 
       , 2))  AS 'HYD_Water_flow_Fresh_water_cooled_60_45_sea' 
	   ,[Extra factor for water flow at 60 humidity fresh] 
	   ,ceiling(Round(((( ( 60 
       - [compressed air temperature above cooling medium temperature c] ) 
         * 
                 [capacity at normal working pressure m min] * 1.2 
             ) / 60 )+ ((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting),1))
               AS 'HYD_After_cooler_heat_rejection_60_45' 
			   ,ROUND([Shaft power at normal working pressure kW]*0.08,1) AS 'HYD_HEAT_DISSSIPATION'
 ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / ( 
       1.2 * 
       [cooling air flow m s] ), 0) AS 'Cooling air temperature rise (C�)' 
	   ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       ( 4.2 * ( [water out c fresh water cooled] 
                 - [water in c fresh water cooled] ) ) 
       , 2) AS 'Water flow (l/s), Fresh water cooled' 
	   ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       ( 4.2 * ( [water out c sea water cooled] - [water in c sea water cooled] 
               ) ), 2) 
       AS 'Water flow (l/s), Sea water cooled' 
	   ,ceiling([shaft power at normal working pressure kw] * 0.75) AS 
       'Oil cooler heat rejection (kW)' 
	   ,(( ( 60 
       - [compressed air temperature above cooling medium temperature c] ) 
         * 
                 [capacity at normal working pressure m min] * 1.2 
             ) / 60 
              ) AS 'After cooler heat rejection (kW)' 
			  ,Concat(Round([shaft power at normal working pressure kw] * 0.95, 2), 
       ' / ', 
              Round([shaft power at normal working pressure kw] * 0.08, 2)) AS 
       'Heat dissipation (kW) EA / EW' 
	   
	  
	   ,round(((([Shaft power at normal working pressure kW]/1000)*1.07)*1000000/(SQRT(3)* [1 Voltage] * [Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current for package440'
	   ,round(((([Shaft power at normal working pressure kW]/1000)*1.07)*1000000/(SQRT(3)* [2 Voltage] * [Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current for package660'
	   ,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage]  + ' V ' as'Voltage_2',
Model_No as 'model_no'
,[Transmission Gear i] as 'Transmission_gear_1'

 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
--where dt.[Datasivun numero]='MDATA3639'
--where 
--CODE='EMH21-44' AND [Capacity at normal working pressure m min] > 0  and [Datasivun numero]='MDATA0002'
--cooling='HW'


GO


