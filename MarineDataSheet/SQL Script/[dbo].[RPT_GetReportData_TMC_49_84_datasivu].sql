USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_TMC_49_84_datasivu]    Script Date: 3/22/2021 11:09:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


----convert(float,replace([Extra capacity],',','') )---EXTRA CAPACITY
ALTER VIEW [dbo].[RPT_GetReportData_TMC_49_84_datasivu]
AS
SELECT
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model'
,Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +'  V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Maximum working pressure barg] AS 'Maximum working pressure barg_Header'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
,(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*60 as 'Capacity_at_normal_working_pressure'
,(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2)) as 'Capacity_at_normal_working_pressure_min'
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1) as 'Shaft_power_at_normal_working_pressure'
,cast([Maximum working pressure barg]  as nvarchar(200)) +' / ' +  cast([Maximum working pressure barg]/10 as nvarchar(200)) as 'Maximum_working_pressure'
,cast([Minimum working pressure barg]  as nvarchar(200)) +' / ' +  cast([Minimum working pressure barg]/10 as nvarchar(200)) as 'Minimum_working_pressure'
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99*0.22 ,1)as 'Idling_shaft_power_consumption'
,case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Male_rotor_speed_min_max'
,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
,[Datasheet revision]
,[Transmission Gear i] AS 'Transmission'
,CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then '6 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,[Maximum cooling air pressure drop Pa] as 'Maximum_cooling_air_pressure_drop_Pa'
,Round(((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/((1.2*[Cooling air flow m s])),0) as 'cooling_air_temperature_rise'
,Round((((((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )))),1) as 'waterFlow_oilcooler_60_60'
,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,Round(((0.75*(Round(Convert(float,replace(convert(float,replace([Specific power consumption kW m min],',','') ),',','') )*
((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*
[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])
*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','')),1) as'waterFlow_oilcooler_sea_40_60'
,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel'
,[Maximum inlet pressure barg] as 'Maximum_inlet_pressure_barg'
,[Water in out T2 T3] as 'water_in_out_t2_t4'
,ROUND((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)*0.75),0)
AS 'oil_cooler_heat_rejection'
,ROUND((((60-[Compressed air temperature above cooling medium temperature C])*(((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2))/60)) +((0.04)*(((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.1)/60)*2260*lt.Weighting),0) as 'After_cooler_heat_rejection'
,((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*0.95)  as 'Heat_dissipation_Ea'
,Round((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100)) +(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02),1) as 'Heat_dissipation_Ew'
,[Main motor F class IP 55 kW] as'Main_motor_class_IP_55'
,[Main motor protection details] as 'Main_motor_protection_details'
,[Main motor max power] as 'Main motor max power'
,[Speed of rotation rpm] as 'Speed of rotation rpm'
,[Fan motor kW EA] as 'Fan motor kW EA'
,[Speed of rotation rpm fan motor] as 'Speed_of_rotation_rpm_fan_motor'
,[Voltage tolerance] as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A]as 'Fuse_max_1_J�nnite_A'
,[Fuse max 2 J�nnite A] as 'Fuse_max_2_J�nnite_A'
,[Starting current Ia In DOL YD SOFT START] AS'Starting_current_Ia_In_DOL_YD_SOFT_START'
,[Control voltage V] AS 'Control_voltage_V'
,Round(((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02))/(1.25*10))+[Cooling air flow m s],2) AS 'Cooling_air_flow_for_entilation_of_compressor_room_1'
,Round((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02))/(1.25*10),2) as 'Cooling_air_flow_for_entilation_of_compressor_room_2'
,Cast([Oil quantity EA I] as nvarchar(200))+' / ' +  Cast([Oil quantity I] as nvarchar(200)) as 'Oil_quantity I'
,[Oil content mg m] as 'Oil_content_mg_m'
,[Air outlet T1] as 'Air outlet T1'
,[Air outlet T1 water cooled] as 'Air_outlet_T1_water'
,Cast([Weight with NOVOX canopy EA] as nvarchar(200)) +' / '+ cast([Weight with NOVOX canopy] as nvarchar(200)) as  'Weight_with_canopy_EA_EW'
,[Condensate drain T4] as 'Condensate_drain_T4'
,[Pressure Level LpA without canopy EW] as 'Pressure_Level_LpA_without_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EW] as 'Pressure_Level_LpA_with_NOVOX_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EA] as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
,[Weight without canopy] as 'Weight without canopy'
,[Weight with NOVOX canopy] as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing SW] as 'General_arrangement_drawing_SW'
,[General arrangement drawing FW] as 'General arrangement_drawing_FW'
,Cast([General arrangement drawing with NOVOX canopy EW] as nvarchar(200)) +' / ' + Cast([General arrangement drawing with NOVOX canopy EW SW] as nvarchar(200)) as 'General_arrangement_drawing_with_NOVOX_canopy_EA_EW'
,[General arrangement drawing with NOVOX canopy EA] as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
,[Cable gland Power] AS 'Cable_gland_Power'
,[Cable gland Alarm Signal] AS 'Cable_gland_Alarm_Signal'
,Lis�tietoja AS 'Extra_notes'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,[Datasheet revision] AS 'Datasheet_revision'
,(Round((((isnull([Fan motor kW EA],0)+(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) as 'Current_for_package_EA'
,case when [Fuse max 2 J�nnite A] >0 then
(Round((((isnull([Fan motor kW EA],0)+(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) else '' end as 'Current_for_package_EA_1'
,(ROUND(((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/((SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100))),0)) 'Current_for_package_EW'
,case when [Fuse max 2 J�nnite A] >0 then
(Round(((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) else '' end as 'Current_for_package_Ew_1'
 ,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
    WHERE CODE= 'TMC49-84' and Model_No in('TMC 49','TMC 64','TMC 84')
--AND [Datasivun numero]='MDATA1988'  
--AND Laskuri=7039
	

GO


