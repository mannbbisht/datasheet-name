USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_Datasivu_UUSI]    Script Date: 3/22/2021 10:28:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData_Datasivu_UUSI]
AS
SELECT Laskuri, [Datasivun numero] , 
[Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg',
Convert(varchar,[Valid from],101) as 'Valid_from' , [1 Voltage] +'  V / ' +  [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
,[Maximum working pressure barg] 'Maximum_working_pressure_barg_header'
, [2 Voltage] +'  V / ' +  [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage',[Normal working pressure barg] as 'Normal_working_pressure_barg'
  ,[Maximum ambient temperature C] as 'Maximum_ambient_temperature'
 , case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Transmissioncalculated'
,Cast([Maximum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Maximum working pressure barg]/10 as nvarchar(20)) as 'Maximum_working_pressure'
,Cast([Minimum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Minimum working pressure barg]/10 as nvarchar(20)) as 'Minimum_working_pressure_barg'
,ROUND((((convert(float,replace([Air end volume per revolution],',','')) * [Speed of rotation rpm] *[Motor pulley]/[Air end pulley]) 
*(convert(float,replace([Volumetric efficiency],',',''))/100))/1000 *((1.02*((100 + case when [Extra capacity] >0 then  [Extra capacity] else 0 end
))/100))*60),1) AS 'Capacity_at_Normal_Working_Pressure'
,ROUND((((convert(float,replace([Air end volume per revolution],',','')) * [Speed of rotation rpm] *[Motor pulley]/[Air end pulley]) 
*(convert(float,replace([Volumetric efficiency],',',''))/100))/1000 *((1.02*((100 + case when [Extra capacity] >0 then  [Extra capacity] else 0 end
))/100))),2) AS 'Capacity_at_Normal_Working_Pressure_min'
,ROUND((((((convert(float,replace([Air end volume per revolution],',','')))*[Speed of rotation rpm] *[Motor pulley]/[Air end pulley]) 
*((convert(float,replace([Volumetric efficiency],',','')))/100))/1000)*0.99) *(convert(float,replace([Specific power consumption kW m min],',',''))),1) as 'Shaft_power_at_normalpressure_barg'
,Round((((((convert(float,replace([Air end volume per revolution],',','')))*[Speed of rotation rpm] *[Motor pulley]/[Air end pulley]) 
*((convert(float,replace([Volumetric efficiency],',','')))/100))/1000)*0.99) *(convert(float,replace([Specific power consumption kW m min],',','')))*0.22,1) as 'Idling_Shaft_Power'
,'5 -' + cast([Compressed air temperature above cooling medium temperature C] as nvarchar(50)) as 'Compressed_air_temperature_above_cooling'
,[Cooling air flow m s]
,[Maximum cooling air pressure drop Pa]
,ROUND(0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))/2.16 + ((60-[Compressed air temperature above cooling medium temperature C])*((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2/60)) /(1.2*[Cooling air flow m s]),1) AS 'Cooling_air_temperature_rise'
,[Cooling air flow m s water cooled] AS 'Cooling_air _low_water_cooled'
,Round((
(0.75 *(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) +((60-[Compressed air temperature above cooling medium temperature C])* (Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2/60)))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])
)*(convert(float,replace([Extra factor for water flow at 60 humidity fresh],',',''))) ,1) AS 'Water_flow_Fresh_water_cooled_60_45' 
,cast([Water in C Fresh water cooled] as nvarchar(200)) +' / ' +  cast([Water out C Fresh water cooled]  as nvarchar(300)) as 'Water_in_water_out_Fresh_water_cooled'
,Round((
(0.75 *(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) +((60-[Compressed air temperature above cooling medium temperature C])* (Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2/60)))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])
)*(convert(float,replace([Extra factor for water flow at 60 humidity fresh],',',''))) ,1) AS 'Water_flow_Fresh_water_cooled_60_45_sea' 
,cast([Water in C Sea water cooled] as nvarchar(200)) + ' / ' + cast([Water out C Sea water cooled] as nvarchar(200)) as 'Water_in_max_water_out_max_Sea_water_cooled'
,cast([Minimum water inlet pressure with zero back bar MPa Serial] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa Serial]/10) as nvarchar(12)) as 'Minimum_water_inlet_pressure_with_MPA_Serial' 
,[Maximum inlet pressure barg] as 'Maximum_inlet_pressure_barg'
,[Water in out T2 T3] as 'Water_in_out_T4_T3'
,(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99 *0.75,0)) as 'Oil_cooler_heat_rejection'
,Round(((((60-[Compressed air temperature above cooling medium temperature C]) *(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*(convert(float,replace([Volumetric efficiency],',','') )/100	)/1000)) *Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2)) *1.2))
/60) +
(0.04*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2)*1.1))/60*2260*lt.Weighting,0) as 'After_coolerheat_rejection_at_60_air_humidity_45_temp'

,Concat(Round((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) * 0.95, 2), 
       ' / ', 
              ((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02))) AS 
       'Heat_dissipation_kW_EA_EW' 
	   ,[Main motor F class IP 55 kW] as 'Main_motor_F_class_IP_55_kW'
	   ,[Main motor max power] as 'Main_motor_max_power'
	   ,[Main motor protection details] as 'Main_motor_protection_details'
	   ,[Main motor temperature rise] as 'Main_motor_temperature_rise'
	   ,[Speed of rotation rpm] as 'Speed_of_rotation_rpm'
	   ,[Fan motor kW EW] as 'Fan_motor_kW_EW'
	   ,[Speed of rotation rpm fan motor] as 'Speed_of_rotation_rpm_fan_motor'
	   ,[Voltage tolerance] as 'Voltage_tolerance'
	   ,[Fuse max 1 J�nnite A] as 'Fuse_max_1_J�nnite_A'
	   ,[Fuse max 2 J�nnite A] as 'Fuse_max 2_J�nnite_A'
	   ,round((((isnull([Fan motor kW EW],0)+ (Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))/1000)*1.07)*1000000/(SQRT(3)* [1 Voltage] * [Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current_for_package440'
	   ,round((((isnull([Fan motor kW EW],0)+ (Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))/1000)*1.07)*1000000/(SQRT(3)* [2 Voltage] * [Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current_for_package660'
	   ,[Starting current Ia In DOL YD SOFT START] as 'Starting_current_Ia_In DOL_YD_SOFT_START'
	   ,[Control voltage V] as 'Control_voltage_V'
	   ,[Oil quantity I] as 'Oil_quantity_I'
	   ,[Oil content mg m] as 'Oil_content_mg'
	   ,[Air outlet T1] as 'Air_outlet_T1'
	   ,[Air outlet T1 water cooled] as 'Air_outlet_T1_water_cooled'
	   ,[Condensate drain T4] as 'Condensate_drain_T4'
	   ,[Weight with NOVOX canopy] as 'Weight_with_NOVOX _anopy'
	   ,[General arrangement drawing with NOVOX canopy EA] as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
	   ,[General arrangement drawing with NOVOX canopy EW] as 'General_arrangement_drawing_with_NOVOX_canopy_Ew'
	   ,[Cable gland Power] as 'cable_gland_Power'
	   ,[Cable gland Alarm Signal] as 'Cable_gland_Alarm_Signal'
	   ,[Pressure Level LpA with NOVOX canopy EA]as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
	   ,[Pressure Level LpA with NOVOX canopy EW]  as 'Pressure_Level_LpA_with_NOVOX_canopy_Ew'
	   ,Lis�tietoja as 'Extra_Comments'
		,[Datasheet revision]
		,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
    WHERE CODE= 'EMH21-44' AND ISNULL([Capacity at normal working pressure m min],'')='' AND ISNULL([Main motor temperature rise],'')=''
--AND [Datasivun numero]='MDATA0692'  
--AND Laskuri=3128

GO


