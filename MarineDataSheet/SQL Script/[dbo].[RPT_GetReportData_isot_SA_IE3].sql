USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_isot_SA_IE3]    Script Date: 4/9/2021 2:03:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData_isot_SA_IE3]
AS
SELECT
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
,CAST(([Capacity at normal working pressure m min min]*60)AS nvarchar(200)) +' / ' +CAST(Round([Capacity at normal working pressure m min]*60,0) AS NVARCHAR(200)) AS 'Capacity_at_normal_working_pressure_min_max'
,CAST((Round([Capacity at normal working pressure m min min],1)) AS nvarchar(200)) +' / ' +CAST(Round([Capacity at normal working pressure m min],1) AS NVARCHAR(200)) AS 'Capacity_at_normal_working_pressure_min_min'
,CAST(Round([Shaft power at normal working pressure kW min],0)AS nvarchar(200)) +' / ' +CAST(([Shaft power at normal working pressure kW]) AS NVARCHAR(200)) AS 'Shaft_power_at_normal_working_pr_min_max'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,CAST(( convert(float,replace([Frequency min],',','') )) AS NVARCHAR(200)) +' / ' +CAST((( convert(float,replace([Frequency max],',','') ))) AS NVARCHAR(200)) as 'Frequency_min_max'
,Round([Shaft power at normal working pressure kW min] *0.22,1) as 'Male_rotor_speed_min_max'
,CASE WHEN Transmission='G' THEN [Transmission Gear i]  END AS 'Transmission'
, '0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then '6 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,Round(((((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ))),1)as'waterFlow_oilcooler'
,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ),1) as'waterFlow_oilcooler_sea'
,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,CAST(Cast([Minimum water inlet pressure with zero back Serial sea] as numeric(10,1)) AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water_in_out_T2_T3'
,Round([Shaft power at normal working pressure kW]*0.75,0) as 'Oil_cooler_heat_rejection'
,Round((((60-[Compressed air temperature above cooling medium temperature C])*([Capacity at normal working pressure m min]*1.2))/60) +((0.04*[Capacity at normal working pressure m min]*1.1/60)*2260*lt.Weighting),0) as 'After_cooler_heat_rejection'
,Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)) +(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02),1) as 'Heat_dissipation_EW'
,[Main motor F class IP 55 kW] as'Main_motor_class_IP_55'
,[Main motor protection details] as 'Main_motor_protection_details'
,[Main motor temperature rise] as 'Main_motor_temperature_rise'
,cast((Round(cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm],0)) as nvarchar(200)) +' / '+ cast((Round(cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm],0)) as nvarchar(200)) as 'Speed_of_rotation_rpm_min_max'
,[Voltage tolerance] as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A] as'Fuse max 1 J�nnite A'
,[Fuse max 2 J�nnite A] as 'Fuse max 2 J�nnite A'
,Round((((isnull([Fan motor kW EW],0) + [Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3))* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package440'
,Round((((isnull([Fan motor kW EW],0) + [Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3))* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package640'
,[Control voltage V] as 'Control_voltage_V'
,Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02))/(1.25*10),2) as 'Cooling_air_flow_for_entilation_of_compressor_room'
,[Oil quantity I] as 'Oil_quantity I'
,[Oil content mg m] as 'Oil_content_mg_m'
,[Air outlet T1 water cooled] as 'Air_outlet_T1'
,[Condensate drain T4] as 'Condensate_drain_T4'
,[Pressure Level LpA without canopy EW] as 'Pressure_Level_LpA_without_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EW] as 'Pressure_Level_LpA_with_NOVOX_canopy_EW'
,[Weight without canopy] as 'Weight without canopy'
,[Weight with NOVOX canopy] as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing SW] as 'General_arrangement_drawing_SW'
,[General arrangement drawing FW] as 'General arrangement_drawing_FW'
,[General arrangement drawing with NOVOX canopy EW] AS 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Cable gland Power] AS 'Cable_gland_Power'
,[Cable gland Alarm Signal] AS 'Cable_gland_Alarm_Signal'
,Lis�tietoja AS 'Extra_notes'
,Round(case when Transmission='B' THEN (((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) * [Motor pulley])/[Air end pulley]) when Transmission='D'
THEN cast((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))
WHEN Transmission='G' THEN cast((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))  *[Transmission Gear i]  END ,0) as 'Transmission1' 
,Round(case when Transmission='B' THEN (((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) * [Motor pulley])/[Air end pulley]) when Transmission='D'
THEN cast((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))
WHEN Transmission='G' THEN cast((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))  *[Transmission Gear i]  END ,0)  as 'Transmission2' 
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
,[Maximum ambient temperature C] as 'maximum_ambient_temperature_C'
 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
    WHERE CODE IN ('TMC105-235SA','TMC150-235SA','TMC240-365SA','TMC400-450SA','TMC245-360SA') AND Cooling='EW' AND convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) >0 AND [Main motor temperature rise] IS NOT NULL
	  --and [Datasivun numero]='MDATA3774'
	

GO


