USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_isot_UUSI]    Script Date: 3/22/2021 10:58:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData_isot_UUSI]
AS
SELECT
Laskuri
,[Datasivun numero] 
,Convert(varchar,[Valid from],101) as 'Valid from'
,[Datasheet revision] as 'Datasheet_revision'
,[Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model'
,[1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
,[2 Voltage] + case when Convert(float,replace([2 Voltage],',','')) > 1 then 'V / ' else ' ' end + [2 Frequency] + case when Convert(float,replace([2 Voltage],',','')) > 1 then ' Hz' else ' ' end +' ' + [2 Starting type] as '2Voltage'
,CAST([Maximum ambient temperature C] AS NVARCHAR(200)) +'C�' as 'Max Amb'
,[Maximum working pressure barg] as 'Maximum working pressure (barg)'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
,round(convert(varchar,[Capacity at normal working pressure m min]*60,101),2) as 'Capacity_at_normal_working_pressure_h'
,round(convert(varchar,[Capacity at normal working pressure m min]),2) as 'Capacity_at_normal_working_pressure_min'
,round(Convert(varchar,([Shaft power at normal working pressure kW]),101),0) as 'Shaft_power_at_normal_working_pr_KW'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,(Round((convert(float,replace([Shaft power at normal working pressure kW],',',''))*0.22),1)) as 'Idling_shaft_power_consumption'
,case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Male_rotor_speed_min_max'
,[Transmission Gear i] as 'Transmission'
,'0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Allowed_ambient_temperature_MIN_MAX'
,case when CAST([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>6 then '6 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200))
else ''+CAST([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end as 'Compressed_air_temperature_above_cooling_medium_temperature'
,Round((((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*
[Capacity at normal working pressure m min]*1.2/60))/(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))*convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','')),1) as 'Waterflow_(l/s),_Fresh_water_cooled_at_ 60%_air_humidity_/_45C_temp_**'
,CAST([Water in C Fresh water cooled] as nvarchar(200)) + case when [Water in C Fresh water cooled]> 0 then ' / ' else ' ' end + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  case when [Minimum water inlet pressure with zero back bar MPa Serial]> 0 then ' / ' else ' ' end + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,Round((((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*
[Capacity at normal working pressure m min]*1.2/60))/(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])))*convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','')),1) as 'Waterflow_(l/s),_Sea_water_cooled_at_ 60%_air_humidity_/_45C_temp_**'
,cast([Water in C Sea water cooled] as nvarchar(200)) +case when [Water in C Sea water cooled]> 0 then ' / ' else ' ' end + Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Sea_water_cooled'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  case when [Minimum water inlet pressure with zero back Serial sea]> 0 then ' / ' else ' ' end + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel_sea'
,[Maximum inlet pressure barg] as 'Maximum_inlet_pressure_barg'
,[Water in out T2 T3] as 'water_in_out_t3_standard'
,ROUND((Convert(float,replace([Shaft power at normal working pressure kW],',',''))*0.75),0) AS 'oil_cooler_heat_rejection'
,ROUND((((60-[Compressed air temperature above cooling medium temperature C])*(([Capacity at normal working pressure m min]*1.2))/60)) +((0.04)*(([Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting),0) as 'After_cooler_heat_rejection'
,Round(((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02)),1) as 'Heat_dissipation_kW_EW'
,[Main motor F class IP 55 kW] as 'Main_motor_nominal_power_kW'
,[Main motor protection details]as 'Main_motor_protection_details'
,[Speed of rotation rpm] as 'Speed_of_rotation_rpm_main_motor_min'
,[Voltage tolerance]as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A]as 'Fuse_max_1_J�nnite_A'
,[Fuse max 2 J�nnite A]as 'Fuse_max_2_J�nnite_A'
,Round((((isnull([Fan motor kW EW],0)+[Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3))* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package440'
,Round((((isnull([Fan motor kW EW],0)+[Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3))* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package640'
,[Starting current Ia In DOL YD SOFT START] as 'Starting_current_Ia_In_DOL_YD_SOFT_START'
,[Control voltage V]as 'Control_voltage_V'
,CAST(ROUND((((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02))/(1.25*10)),2) AS NVARCHAR(200)) as 'Cooling_air_flow_for_ventilation_of_compressor_room_m3_s'
,[Oil quantity I]as 'Oil_quantity_I'
,[Oil content mg m]as 'Oil_content_mg_m'
,[Air outlet T1 water cooled]as 'Air_outlet_T1_water_cooled'
,[Condensate drain T4]as 'Condensate_drain_T4'
,[Weight without canopy] as 'Weight_without_canopy'
,[Weight with NOVOX canopy]as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing SW] as 'General_arrangement_drawing_SW'
,[General arrangement drawing FW]as 'General_arrangement_drawing_FW'
,[General arrangement drawing with NOVOX canopy EW]as 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Cable gland Power]as 'Cable_gland_Power'
,[Cable gland Alarm Signal]as 'Cable_gland_Alarm_Signal'
,[Pressure Level LpA without canopy EW]as 'Pressure_Level_LpA with_out_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EW]as 'Pressure_Level_LpA_with_NOVOX_canopy_EW'
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,Lis�tietoja AS 'Extra_notes'
,[1 Voltage] + ' V ' as'Voltage_1'
	   ,[2 Voltage] + ' V ' as'Voltage_2'
	   ,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
FROM  dbo.Data 
dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
WHERE 
(CODE in('TMC105-235','TMC105-235SL','TMC240-365','ULM90-200','TMC400-450')
AND [Cooling] = 'EW' 
 AND convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) >0 
AND([Main motor temperature rise] IS NULL) OR (Code='TMC245-360' AND [Cooling] = 'EW' AND convert(float,replace([Extra factor for water flow at 60 humidity fresh],',',''))>0 AND [Main motor temperature rise] IS NULL) ) 
--AND [Datasivun numero]='MDATA0360' 
--and Laskuri=3452



GO


