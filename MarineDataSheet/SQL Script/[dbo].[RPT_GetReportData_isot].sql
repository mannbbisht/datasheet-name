USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_isot]    Script Date: 3/22/2021 10:36:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData_isot]
AS
SELECT Laskuri, [Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +'  V / ' +  [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +'  V / ' +  [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage',[Normal working pressure barg] as 'Normal working pressure barg',
[Capacity at normal working pressure m min] * 60 as 'Capacity at normal working pressure m min calculated' ,Cast([Capacity at normal working pressure m min] as numeric(10,2)) as 'Capacity at normal working pressure m min'
,[Shaft power at normal working pressure kW] as 'Shaft power at normal working pressure kW'
,Cast([Maximum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Maximum working pressure barg]/10 as nvarchar(20)) as 'Maximum working pressure barg(mPag)'
,Cast([Minimum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Minimum working pressure barg]/10 as nvarchar(20)) as 'Minimum working pressure barg(mPag)'
,round([Shaft power at normal working pressure kW]*0.22,1) as 'Shaft power at normal working pressure kW calculated'
,[Transmission Gear i]  as 'Tranmission', [Speed of rotation rpm],
case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Transmissioncalculated',
[Motor pulley],[Air end pulley],
case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus',cast('0' as nvarchar(20))+cast(' - ' as nvarchar(12)) +cast([Maximum ambient temperature C] as nvarchar(12)) as 'Allowed ambient temperature MIN MAX C'
,cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12)) as 'Compressed air temperature above cooling medium temperature C'
,[Cooling air flow m s] as 'Cooling air flow m s',[Maximum cooling air pressure drop Pa] as 'Maximum cooling air pressure drop Pa'
,[Water in C Fresh water cooled] as 'Water in C Fresh water cooled'
,[Water out C Fresh water cooled] as 'Water out C Fresh water cooled'
,[Water in C Sea water cooled] as 'Water in C Sea water cooled'
,[Water out C Sea water cooled] as 'Water  Out C Sea water cooled'
,cast([Minimum water inlet pressure with zero back bar MPa Serial] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa Serial]/10) as nvarchar(12)) as 'Minimum water inlet pressure with zero ba bar MPa Parallel' 
,cast([Minimum water inlet pressure with zero ba bar MPa Parallel] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero ba bar MPa Parallel]/10) as nvarchar(12))  as 'Minimum water inlet pressure with zero back bar MPa After'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water in out T2 T3'
,[Main motor F class IP 55 kW] as 'Main motor F class IP 55 kW'
,cast([Fan motor kW EA] as nvarchar(max))  +cast(' / ' as nvarchar(10)) + cast([Fan motor kW EW] as nvarchar(max)) as 'Fan motor kW EA / EW'
,[Speed of rotation rpm fan motor] as 'Speed of rotation rpm fan motor'
,[Fuse max 1 J�nnite A]
,[Fuse max 2 J�nnite A]
,[Motor cos fii]
,[Motor efficiency]
,[Starting current Ia In DOL YD SOFT START]
,[Control voltage V]
,[Oil quantity I]
,[Oil content mg m]
,[Oil quantity EA I]
,[Air outlet T1]
,[Condensate drain T4]
,[Pressure Level LpA with NOVOX canopy EA]
,[Pressure Level LpA with NOVOX canopy EW]
,[Pressure Level LpA without canopy EA]
,[Pressure Level LpA without canopy EW]
,[Weight without canopy]
,[Weight with NOVOX canopy]
,[Weight with NOVOX canopy EA]
,[General arrangement drawing EA]
,[General arrangement drawing FW]
,[General arrangement drawing SW]
,[General arrangement drawing with NOVOX canopy EA]
,[General arrangement drawing with NOVOX canopy EW]
,[Cable gland Power]
,[Cable gland Alarm Signal]
,Lis�tietoja as 'Extra notes'
,[Datasheet revision]
,[Maximum working pressure barg] as 'Maximum working pressure barg single row'
,[Shaft power at normal working pressure kW] *0.75 as 'Oil_cooler_heat)rejection'
,Round((((60-[Compressed air temperature above cooling medium temperature C]) *([Capacity at normal working pressure m min] *1.2))/60),0) as 'After_cooler_heat_rejection'
,CAST(ROUND(([Shaft power at normal working pressure kW]) * 0.08,1) AS NVARCHAR(12)) as 'Heat dissipation'
 ,ROUND(( 0.75 *[Shaft power at normal working pressure kW]+(((60-[Compressed air temperature above cooling medium temperature C]) *([Capacity at normal working pressure m min]*1.2/60))))/(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])),2) AS'Water_flow_Fresh_ater_cooled'
 ,ROUND(( 0.75 *[Shaft power at normal working pressure kW]+(((60-[Compressed air temperature above cooling medium temperature C]) *([Capacity at normal working pressure m min]*1.2/60))))/(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])),2) AS'Water_flow_Fresh_ater_cooled_SEA'
 ,Round((((isnull([Fan motor kW EW],0)+ [Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3)* [1 Voltage] * [Motor cos fii])*([Motor efficiency]/100)),0) as'Current_for_package_1_voltage'
,Round((((isnull([Fan motor kW EW],0)+ [Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3)* [2 Voltage] * [Motor cos fii])*([Motor efficiency]/100)),0) as'Current_for_package_2_voltage'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
 FROM  dbo.Data 
where 
   code in('TMC105-235','TMC105-235SL','TMC240-365','ULM90-200','','','') and cooling='EW' and [Extra factor for water flow at 60 humidity fresh] is  null
--Cooling='HW'



GO


