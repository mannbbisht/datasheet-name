USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_TMC_86_124_datasivu]    Script Date: 3/22/2021 11:17:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



----convert(float,replace([Extra capacity],',','') )---EXTRA CAPACITY
ALTER VIEW [dbo].[RPT_GetReportData_TMC_86_124_datasivu]
AS
select
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model'
,Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Maximum working pressure barg] AS 'Maximum working pressure barg_Header'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
,[Datasheet revision] as 'Datasheet_revision'
,Round((((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]* [Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	1.02*(100+isnull([Extra capacity],0))/100) *60,1) as 'Capacity_at_normal_working_pressure'
,Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	1.02*(100+isnull([Extra capacity],0))/100,2) as 'Capacity_at_normal_working_pressure_min'
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1) AS 'Shaft_power_at_normal_working_pr_min_max'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,Round(((Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99)) *0.22,1) as 'Idling_shaft_power_consumption'
,case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Male_rotor_speed_min_max'
,CASE WHEN Transmission='G' THEN [Transmission Gear i]  END AS 'Transmission'
, '0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then '6 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,[Maximum cooling air pressure drop Pa]
,Round(((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/((1.2*[Cooling air flow m s])),0) as 'cooling_air_temperature_rise'
,Round((((((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm] *[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )))),1)as'Water_flow_Fresh_water_cooled_at_air_humidity_temp'
,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel' 
,Round((((((0.75*(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99))+((60-[Compressed air temperature above cooling medium temperature C])*(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm] *[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	1.02*(100+isnull([Extra capacity],0))/100)*1.2/60))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )))),1) AS 'Water_flow_Fresh_SEA_cooled_at_air_humidity_temp'
,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial_sea'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel_sea'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water_in_out_T2_T3'
,Round((((60-[Compressed air temperature above cooling medium temperature C])*(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm] *[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	1.02*(100+isnull([Extra capacity],0))/100)*1.2)/60) +(((0.04*(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm] *[Transmission Gear i]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	1.02*(100+isnull([Extra capacity],0))/100)*1.1)/60)*2260*lt.Weighting),0) as 'After_cooler_heat_rejection_at_air_humidity_temp'
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*
((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])
*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99*0.75,0) as 'Oil_cooler_heat_rejection'
,((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*0.95) as 'Heat_dissipation_EW_1'
,Round((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100)) +(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02),1)as 'Heat_dissipation_EW_2'
,[Main motor F class IP 55 kW] as'Main_motor_class_IP_55'
,[Main motor max power] as 'Main_motor_max_power'
,[Main motor protection details] as 'Main_motor_protection_details'
,[Speed of rotation rpm] as 'Speed_of_rotation_rpm'
,[Main motor temperature rise] as 'Main_motor_temperature_rise'
,[Fan motor kW EA] as 'Fan_moto_kw_ea'
,[Speed of rotation rpm fan motor] as 'speed_of_rotation_fan_motor'
,[Voltage tolerance] as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A] as'Fuse max 1 J�nnite A'
,[Fuse max 2 J�nnite A] as 'Fuse max 2 J�nnite A'
,(Round((((isnull([Fan motor kW EA],0)+(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) as 'Current_for_package_EA'
,case when [Fuse max 2 J�nnite A] >0 then
(Round((((isnull([Fan motor kW EA],0)+(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) else '' end as 'Current_for_package_EA_1'
,(ROUND(((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/((SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100))),0)) 'Current_for_package_EW'
,case when [Fuse max 2 J�nnite A] >0 then
(Round(((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) else '' end as 'Current_for_package_Ew_1'
,[Control voltage V] as 'Control_voltage_V'
,[Starting current Ia In DOL YD SOFT START] as 'Starting_current_Ia_In_DOL_YD_SOFT_START'
,CAST([Oil quantity EA I] AS NVARCHAR(100)) + ' / '  + cast([Oil quantity I] as nvarchar(100))
as 'Oil_quantity_I'
,[Oil content mg m]as 'Oil_content_mg_m'
,[Air outlet T1]as 'Air_outlet_T1'
,[Air outlet T1 water cooled]as 'Air_outlet_T1_water_cooled'
,[Condensate drain T4]as 'Condensate_drain_T4'
,[Weight with NOVOX canopy]as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing with NOVOX canopy EA]as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
,[General arrangement drawing with NOVOX canopy EW]as 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Cable gland Power]as 'Cable_gland_Power'
,[Cable gland Alarm Signal]as 'Cable_gland_Alarm_Signal'
,[Pressure Level LpA with NOVOX canopy EA]as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
,[Pressure Level LpA with NOVOX canopy EW]as 'Pressure_Level_LpA with_NOVOX_canopy_EW'
,[Pressure Level LpA without canopy EA] as 'Pressure Level LpA without canopy EA'
,[Pressure Level LpA without canopy EW] as 'Pressure Level LpA without canopy EW'
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
,Lis�tietoja AS 'Extra_notes'
,[Maximum ambient temperature C] as 'maximum_ambient_temperature_hader'
,((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) 
*(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))) *(0.95) as 'Dissipation_heat_1'
,(case when [Water in C Fresh water cooled] > 0 then ((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) 
*(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))) end )*(0.08) as 'Dissipation_heat_2'
,[General arrangement drawing FW] as 'General_arrangement_drawing_FW'
,[General arrangement drawing SW] as 'General_arrangement_drawing_SW'
,[General arrangement drawing EA] as 'General_arrangement_drawing_EA'
,[Weight without canopy] as 'Weight_without_canopy'
,[Weight with NOVOX canopy EA] as 'Weight_with_NOVOX_canopy_EA'
,Round((((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02))/((1.25*10))) +[Cooling air flow m s],2) as 'Cooling_air_flow_ms_1'
,Round((((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Transmission Gear i])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02))/((1.25*10))),2) as 'Cooling_air_flow_ms_2'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
FROM  dbo.Data 
dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
    WHERE CODE= 'TMC86-124' and Model_No <> 'TMC 95 SA'
--AND [Datasivun numero]='MDATA1182'  
--AND Laskuri=4494

GO


