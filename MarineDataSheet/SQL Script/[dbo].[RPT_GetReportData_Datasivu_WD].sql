USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_Datasivu_WD]    Script Date: 3/30/2021 10:20:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
----convert(float,replace([Extra capacity],',','') )---EXTRA CAPACITY
ALTER VIEW [dbo].[RPT_GetReportData_Datasivu_WD]
AS
SELECT
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model'
,Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +'  V / ' +  [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +'  V / ' +  [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Maximum working pressure barg] AS 'Maximum working pressure barg_Header'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
, ([Capacity at normal working pressure m min]*60) AS 'Capacity_at_normal_working_pressure_min_max'
,CAST(CAST(([Capacity at normal working pressure m min]) as decimal(10,2)) as nvarchar(50)) AS 'Capacity_at_normal_working_pressure_min_min'
,([Shaft power at normal working pressure kW]) AS 'Shaft_power_at_normal_working_pr_min_max'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,Round(([Shaft power at normal working pressure kW] *0.22),1) as 'Idling_shaft_power_consumption'
,case when Transmission ='B'THEN (Round((([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]),0))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Male_rotor_speed_min_max'
,CASE WHEN Transmission='B' THEN 'Belt'  when Transmission='D' THEN 'Direct' WHEN Transmission='G' THEN  ''   END AS 'Transmission'
, '0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,[Cooling air flow m s] as 'Cooling_air_flow'
,[Maximum cooling air pressure drop Pa] as 'Maximum_cooling_air_pressure_drop_Pa'
,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*([Capacity at normal working pressure m min]*1.2/60)))/(1.2*[Cooling air flow m s]),0) as 'cooling_air_temperature_rise'
,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*([Capacity at normal working pressure m min]*1.2/60)))/(4.2* ([Water out C Fresh water cooled]-[Water in C Fresh water cooled])),2) as 'Water_in_water_out_Fresh_water_cooled'
--,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*([Capacity at normal working pressure m min]*1.2/60)))/(4.2* ([Water out C Sea water cooled]-[Water in C Sea water cooled])),2) as 'Water_in_sea_out_Fresh_water_cooled'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,Floor(((0.75*(Round(Convert(float,replace(convert(float,replace([Specific power consumption kW m min],',','') ),',','') )*
((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*
[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])
*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )) as'waterFlow_oilcooler_sea'
,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,cast([Water in C Sea water cooled] as nvarchar (200)) +' / ' + cast([Water out C Sea water cooled] as nvarchar(200)) as 'water_in_out_sea'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel'
,[Maximum inlet pressure barg] as 'Maximum_inlet_pressure_barg'
,[Water in out T2 T3] as 'water_in_out_t2_t4'
,Round(([Shaft power at normal working pressure kW]*0.75),0) AS 'oil_cooler_heat_rejection'
,((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2)/60 as 'After_cooler_heat_rejection'
,Cast(Round(([Shaft power at normal working pressure kW]*0.95),1) as nvarchar(200)) +' / ' +Cast(case when [Water in C Fresh water cooled] >0 then [Shaft power at normal working pressure kW]*0.08 else '' end as nvarchar(200)) 'Heat_dissipation_EA_EW'
,[Main motor F class IP 55 kW] as 'Main_motor_F_class_IP_55_kW'
,[Main motor max power]as 'Main_motor_max_power'
,[Main motor protection details]as 'Main_motor_protection_details'
,[Speed of rotation rpm]as 'Speed_of_rotation_rpm'
,cast([Fan motor kW EA] as nvarchar(200))  + case when ([Fan motor kW EA]>0 or [Fan motor kW EW]>0) then ' / ' else '' end  + cast([Fan motor kW EW] as nvarchar(200)) as 'Fan_motor_Ea_EW'
,[Speed of rotation rpm fan motor]as 'Speed_of_rotation_rpm_fan_motor'
,[Voltage tolerance]as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A]as 'Fuse_max_1_J�nnite_A'
,[Fuse max 2 J�nnite A]as 'Fuse_max_2_J�nnite_A'
,Round((((isnull([Fan motor kW EW],0)+[Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100))),0) as 'Current_Package_440'
,ROUND((((isnull([Fan motor kW EW],0)+[Shaft power at normal working pressure kW])*1000)*1.07)/((SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100))),0)  as 'Current_Package_660'
,[Starting current Ia In DOL YD SOFT START]as 'Starting_current_Ia_In_DOL_YD_SOFT_START'
,[Control voltage V]as 'Control_voltage_V'
,[Oil quantity I]as 'Oil_quantity_I'
,[Oil content mg m]as 'Oil_content_mg_m'
,[Air outlet T1]as 'Air_outlet_T1'
,[Air outlet T1 water cooled]as 'Air_outlet_T1_water_cooled'
,[Condensate drain T4]as 'Condensate_drain_T4'
,[Weight with NOVOX canopy]as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing with NOVOX canopy EA]as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
,[General arrangement drawing with NOVOX canopy EW]as 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Cable gland Power]as 'Cable_gland_Power'
,[Cable gland Alarm Signal]as 'Cable_gland_Alarm_Signal'
,[Pressure Level LpA with NOVOX canopy EA]as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
,[Pressure Level LpA with NOVOX canopy EW]as 'Pressure_Level_LpA with_NOVOX_canopy_EW'
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
,Lis�tietoja AS 'Extra_notes'
,[Maximum ambient temperature C] as 'maximum_ambient_temperature_hader'
,((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) 
*(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))) *(0.95) as 'Dissipation_heat_1'
,(case when [Water in C Fresh water cooled] > 0 then ((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) 
*(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))) end )*(0.08) as 'Dissipation_heat_2'
,cast([Water in C Fresh water cooled] as nvarchar(200)) +' / ' + cast([Water out C Fresh water cooled] as nvarchar(300)) as 'Combine_water_in_out'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
FROM  dbo.Data 
    WHERE CODE in('WD7-22A','WD21-44','WD54-85') 
--AND [Datasivun numero]='MDATA3091'  
--AND Laskuri=4760
	

GO


