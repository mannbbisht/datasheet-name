USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_isot_RRM_IE3]    Script Date: 3/22/2021 10:45:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER VIEW [dbo].[RPT_GetReportData_isot_RRM_IE3]
AS
SELECT Laskuri, [Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage',[Normal working pressure barg] as 'Normal working pressure barg',
[Capacity at normal working pressure m min] * 60 as 'Capacity at normal working pressure m min calculated' ,[Capacity at normal working pressure m min] as 'Capacity at normal working pressure m min'
,[Shaft power at normal working pressure kW] as 'Shaft power at normal working pressure kW'
,Cast([Maximum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Maximum working pressure barg]/10 as nvarchar(20)) as 'Maximum working pressure barg(mPag)'
,Cast([Minimum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Minimum working pressure barg]/10 as nvarchar(20)) as 'Minimum working pressure barg(mPag)'
,round([Shaft power at normal working pressure kW]*0.22,1) as 'Shaft power at normal working pressure kW calculated'
,[Transmission Gear i]  as 'Tranmission', [Speed of rotation rpm],
case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Transmissioncalculated',
[Motor pulley],[Air end pulley],
case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus',cast('0' as nvarchar(20))+cast(' - ' as nvarchar(12)) +cast([Maximum ambient temperature C] as nvarchar(12)) as 'Allowed ambient temperature MIN MAX C'
,CASE WHEN [Compressed air temperature above cooling medium temperature C] >6 THEN '6 - ' + cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12))ELSE cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12)) END as 'Compressed air temperature above cooling medium temperature C'
,[Cooling air flow m s] as 'Cooling air flow m s',[Maximum cooling air pressure drop Pa] as 'Maximum cooling air pressure drop Pa'
,[Water in C Fresh water cooled] as 'Water in C Fresh water cooled'
,[Water out C Fresh water cooled] as 'Water out C Fresh water cooled'
,[Water in C Sea water cooled] as 'Water in C Sea water cooled'
,[Water out C Sea water cooled] as 'Water  Out C Sea water cooled'
,cast([Minimum water inlet pressure with zero back bar MPa Serial] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa Serial]/10) as nvarchar(12)) as 'Minimum water inlet pressure with zero ba bar MPa Parallel' 
,cast([Minimum water inlet pressure with zero back bar MPa After] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa After]/10) as nvarchar(12))  as 'Minimum water inlet pressure with zero back bar MPa After'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water in out T2 T3'
,[Main motor F class IP 55 kW] as 'Main motor F class IP 55 kW'
,[Fan motor kW EA]  as 'Fan_motor_EA'
,[Main motor protection details]
,[Main motor temperature rise]
,[Voltage tolerance]
,[Speed of rotation rpm fan motor] as 'Speed of rotation rpm fan motor'
,[Fuse max 1 J�nnite A]
,[Fuse max 2 J�nnite A]
,[Motor cos fii]
,[Motor efficiency]
,[Starting current Ia In DOL YD SOFT START]
,[Control voltage V]
,[Condensate drain T4]
,[Weight without canopy]
,[Weight with NOVOX canopy]
,[Weight with NOVOX canopy EA]
,[General arrangement drawing EA]
,[Maximum working pressure barg] as 'Maximum working pressure barg single row'
,ceiling(Round(((( ( 60 
       - [compressed air temperature above cooling medium temperature c] ) 
         * 
                 [capacity at normal working pressure m min] * 1.2 
             ) / 60 )+ ((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting),1))
               AS 'After_cooler_heat_rejection_60_45' 
,CAST(ROUND(([Shaft power at normal working pressure kW]) * 0.08,1) AS NVARCHAR(12)) as 'Heat dissipation'
 ,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])),1) as'waterFlow_oilcooler'
 ,Code
  
	   ,Round(( 0.75 *[Shaft power at normal working pressure kW]+(((60-[Compressed air temperature above cooling medium temperature C]) *([Capacity at normal working pressure m min]*1.2/60))))/(1.2*[Cooling air flow m s]),0) as 'Cooling_air_temperature_rise'
	   ,[Oil quantity I]
,[Oil content mg m]
,[Oil quantity EA I]
,[Air outlet T1]
	  , [Air outlet T1 water cooled]
	   ,[Weight with NOVOX canopy EA] +' / ' + cast([Weight with NOVOX canopy] as nvarchar(300)) as 'Weight_without_canopy'
	   ,cast([General arrangement drawing SW] as nvarchar(300)) as 'General_arrangement_drawing_without_canopy_FW_SW'
	   ,[General arrangement drawing with NOVOX canopy EA] +' / ' + cast([General arrangement drawing with NOVOX canopy EW] as nvarchar(300)) as 'General_arrangement_drawing_with_canopy_EA_EW'
	   ,[Cable gland Power]
       ,[Cable gland Alarm Signal]
	   ,[Pressure Level LpA with NOVOX canopy EA]
		,[Pressure Level LpA with NOVOX canopy EW]
		,[Pressure Level LpA without canopy EA]
		,[Pressure Level LpA without canopy EW]
		,Lis�tietoja as 'Extra notes'
,[Datasheet revision]
,Round(((((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2)/60)
+(((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting))/(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','')),0) as 'Oil_cooler_heat_rejection'
,Round(((((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2)/60)) +((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*9 ,0)as'After_cooler_heat_rejection_at_60_air_humidity_45_temp'
,Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(([Shaft power at normal working pressure kW]*100/[Motor efficiency]))*0.02,0) as 'Heat_dissipation_EW'
,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package440'
 ,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package640'
 ,Round(((((([Shaft power at normal working pressure kW]*100/[Motor efficiency])) *((100-[Motor efficiency])/100)) +(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02) )/(1.25*10)),2) as 'Cooling_air_flow_for_ventilation_of_compressor_room'
 ,[General arrangement drawing FW] as 'General_arrangement_drawing_FW'
 ,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
,[Maximum ambient temperature C]
 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
    WHERE CODE IN ('RRM105-185') AND
	 [Main motor temperature rise] IS NOT NULL 
	  --and [Datasivun numero]='MDATA1157'
	

GO


