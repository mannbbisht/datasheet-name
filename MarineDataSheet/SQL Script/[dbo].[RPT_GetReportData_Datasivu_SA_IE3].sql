USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_Datasivu_SA_IE3]    Script Date: 3/22/2021 10:09:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[RPT_GetReportData_Datasivu_SA_IE3]
AS
SELECT
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
,CAST(([Capacity at normal working pressure m min min]*60)AS nvarchar(200)) +' / ' +CAST(([Capacity at normal working pressure m min]*60) AS NVARCHAR(200)) AS 'Capacity_at_normal_working_pressure_min_max'
,CAST(([Capacity at normal working pressure m min min])AS nvarchar(200)) +' / ' +CAST(([Capacity at normal working pressure m min]) AS NVARCHAR(200)) AS 'Capacity_at_normal_working_pressure_min_min'
,CAST(([Shaft power at normal working pressure kW min])AS nvarchar(200)) +' / ' +CAST(([Shaft power at normal working pressure kW]) AS NVARCHAR(200)) AS 'Shaft_power_at_normal_working_pr_min_max'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,CAST(( convert(float,replace([Frequency min],',',''))) AS NVARCHAR(200)) +' / ' +CAST((( convert(float,replace([Frequency max],',','') ))) AS NVARCHAR(200)) as 'Frequency_min_max'
,CAST(CAST(([Shaft power at normal working pressure kW min] *0.22) as DECIMAL(9,1))as nvarchar(50)) as 'Male_rotor_speed_min_max'
,CASE WHEN Transmission='G' THEN [Transmission Gear i]  END AS 'Transmission'
, '0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then '5 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,[Cooling air flow m s] as 'Cooling_air_flow_m_s'
,[Maximum cooling air pressure drop Pa] as 'Maximum_cooling_air_pressure_drop_Pa'
,Round(((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/((1.2*[Cooling air flow m s])),0) as 'cooling_air_temperature_rise'
,ROUND(((((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ))),1)as'waterFlow_oilcooler'
,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,ROUND(((((0.75*[Shaft power at normal working pressure kW])+((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2/60))/
(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ))),1) as'waterFlow_oilcooler_sea'
,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water_in_out_T2_T3'
,Round([Shaft power at normal working pressure kW]*0.75,0) as 'Oil_cooler_heat_rejection'
,Round(ROUND((((60-[Compressed air temperature above cooling medium temperature C])*(([Capacity at normal working pressure m min]*1.2))/60)),0)+(((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting ),0)as 'After_cooler_heat_rejection'
,cast(ROUND(([Shaft power at normal working pressure kW]*0.95),1) as nvarchar(200))+' / '+Cast(Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)) +(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02),1) as nvarchar(200))as 'Heat_dissipation_EW'
,[Main motor F class IP 55 kW] as'Main_motor_class_IP_55'
,[Main motor protection details] as 'Main_motor_protection_details'
,[Main motor temperature rise] as 'Main_motor_temperature_rise'
,cast((Round(cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm],0)) as nvarchar(200)) +' / '+ cast((Round(cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm],0)) as nvarchar(200)) as 'Speed_of_rotation_rpm_min_max'
,[Fan motor kW EA] as 'Fan_moto_kw_ea'
,[Speed of rotation rpm fan motor] as 'speed_of_rotation_fan_motor'
,[Voltage tolerance] as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A] as'Fuse max 1 J�nnite A'
,[Fuse max 2 J�nnite A] as 'Fuse max 2 J�nnite A'
,cast((Round((((isnull([Fan motor kW EA],0)+[Shaft power at normal working pressure kW])*1000)*1.07)/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0)) as nvarchar(200))  AS'Current_for_package440'
,[Control voltage V] as 'Control_voltage_V'
,Cast(Round(((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02))/(1.25*10))+[Cooling air flow m s],2) as nvarchar(200))+' / '+Cast(Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02))/(1.25*10),2) as nvarchar(200)) as 'Cooling_air_flow_for_entilation_of_compressor_room'
,[Oil quantity I] as 'Oil_quantity I'
,[Oil content mg m] as 'Oil_content_mg_m'
,[Air outlet T1] as 'Air outlet T1'
,[Air outlet T1 water cooled] as 'Air_outlet_T1_water'
,Cast([Weight with NOVOX canopy EA] as nvarchar(200)) +' / '+ cast([Weight with NOVOX canopy] as nvarchar(200)) as  'Weight_with_canopy_EA_EW'
,[Condensate drain T4] as 'Condensate_drain_T4'
,[Pressure Level LpA without canopy EW] as 'Pressure_Level_LpA_without_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EW] as 'Pressure_Level_LpA_with_NOVOX_canopy_EW'
,[Pressure Level LpA with NOVOX canopy EA] as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
,[Weight without canopy] as 'Weight without canopy'
,[Weight with NOVOX canopy] as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing SW] as 'General_arrangement_drawing_SW'
,[General arrangement drawing FW] as 'General arrangement_drawing_FW'
,Cast([General arrangement drawing with NOVOX canopy EA] as nvarchar(200)) +' / ' + Cast([General arrangement drawing with NOVOX canopy EW] as nvarchar(200)) as 'General_arrangement_drawing_with_NOVOX_canopy_EA_EW'
,[General arrangement drawing with NOVOX canopy EA] as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
,[General arrangement drawing with NOVOX canopy EW] as 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Uncertainty Kpa Kwa] AS 'Uncertainty_Kpa_Kwa'
,[Cable gland Power] AS 'Cable_gland_Power'
,[Cable gland Alarm Signal] AS 'Cable_gland_Alarm_Signal'
,Lis�tietoja AS 'Extra_notes'
,Round(case when Transmission='B' THEN (((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) * [Motor pulley])/[Air end pulley]) when Transmission='D'
THEN cast((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))
WHEN Transmission='G' THEN cast((cast(( convert(float,replace([Frequency min],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))  *[Transmission Gear i]  END ,0) as 'Transmission1' 
,Round(case when Transmission='B' THEN (((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) * [Motor pulley])/[Air end pulley]) when Transmission='D'
THEN cast((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))
WHEN Transmission='G' THEN cast((cast(( convert(float,replace([Frequency max],',','') )) as float(12))/[1 Frequency]*[Speed of rotation rpm]) as nvarchar(200))  *[Transmission Gear i]  END ,0)  as 'Transmission2' 
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
WHERE CODE IN ('TMC20-40SA','TMC30-40SA','TMC60-80SA','VS40','VS60-80','TMC35-44','TMC 35 SA','TMC 44 SA') AND convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) >0  AND ISNULL([Main motor temperature rise],'')<>''
--AND [Datasivun numero]='MDATA3940' 
--and Laskuri=2165


GO


