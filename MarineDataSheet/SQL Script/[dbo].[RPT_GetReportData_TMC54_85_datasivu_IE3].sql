USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_TMC54_85_datasivu_IE3]    Script Date: 7/5/2021 8:29:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
----convert(float,replace([Extra capacity],',','') )---EXTRA CAPACITY
ALTER VIEW [dbo].[RPT_GetReportData_TMC54_85_datasivu_IE3]
AS
SELECT
Laskuri
,[Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg_model'
,Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage'
,[Normal working pressure barg] AS 'Normal_working_pressure_barg'
, ((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2) )*60) AS 'Capacity_at_normal_working_pressure_min_max'
,(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2)) AS 'Capacity_at_normal_working_pressure_min_min'
,Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1) AS 'Shaft_power_at_normal_working_pr_min_max'
,CAST([Maximum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Maximum working pressure barg]/10) AS NVARCHAR(200)) as 'Maximum_working_pressure_barg'
,CAST([Minimum working pressure barg] AS NVARCHAR(200)) +' / ' +CAST(([Minimum working pressure barg]/10) AS NVARCHAR(200)) as 'Minimum_working_pressure_barg'
,Round(((Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99) *0.22),1) as 'Idling_shaft_power_consumption'
,case when Transmission ='B'THEN ((([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Male_rotor_speed_min_max'
,CASE WHEN Transmission='G' THEN [Transmission Gear i]  END AS 'Transmission'
, '0 - '+CAST([Maximum ambient temperature C] AS NVARCHAR(200)) AS 'Maximum_ambient_temperature'
,case when cast([Compressed air temperature above cooling medium temperature C] as nvarchar(100))>0 then '5 - '+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) else ''+cast([Compressed air temperature above cooling medium temperature C] as nvarchar(200)) end AS 'Compressed_air_temperature_above_cooling_medium_temperature'
,[Cooling air flow m s] as 'Cooling_air_flow'
,[Maximum cooling air pressure drop Pa] as 'Maximum_cooling_air_pressure_drop_Pa'
,Round(((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/((1.2*[Cooling air flow m s])),0) as 'cooling_air_temperature_rise'
,[Cooling air flow m s water cooled]
,Round((((((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )))),1)as'waterFlow_oilcooler_440'
,Cast([Water in C Fresh water cooled] as nvarchar(200)) + ' / ' + Cast([Water out C Fresh water cooled] as nvarchar(200)) as 'Water_in_water_out_Fresh_water_cooled'
,CAST([Minimum water inlet pressure with zero back bar MPa Serial] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back bar MPa Serial]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial'
--,(((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])
--*(Round(((
--convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
--*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
--)) *	Round(1.02*Round((100+isnull([Extra capacity],0))/100,2),2),2))*1.2/60))/
--(4.2*([Water out C Sea water cooled]-[Water in C Sea water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )) as'waterFlow_oilcooler_sea'

,Round((((((0.75*(Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))+((60-[Compressed air temperature above cooling medium temperature C])*(Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2/60))/
(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])))* (convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') )))),0)as'waterFlow_oilcooler_sea'

,cast([Water in C Sea water cooled] as nvarchar(200)) +' / '+ Cast([Water out C Sea water cooled] as nvarchar(200)) as'Water_in_water_out_Fresh_water_cooled_sea'
,cast([Water in C Sea water cooled] as nvarchar (200)) +' / ' + cast([Water out C Sea water cooled] as nvarchar(200)) as 'water_in_out_sea'
,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(200)) +  ' / ' + CAST([Minimum water inlet pressure with zero back Serial sea]/10 AS NVARCHAR(200)) AS 'Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel'
,[Maximum inlet pressure barg] as 'Maximum_inlet_pressure_barg'
,[Water in out T2 T3] as 'water_in_out_t2_t4'
,ROUND((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)*0.75),0) AS 'oil_cooler_heat_rejection'
,ROUND((((60-[Compressed air temperature above cooling medium temperature C])*(((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.2))/60)) +((0.04)*(((Round(((
convert(float,replace([Air end volume per revolution],',','') ) * ([Speed of rotation rpm]*[Motor pulley]/[Air end pulley]) 
*( convert(float,replace([Volumetric efficiency],',','') )/100	)/1000
)) *	Round(1.02*Round((100+isnull(convert(float,replace([Extra capacity],',','') ),0))/100,2),2),2))*1.1)/60)*2260*lt.Weighting),0) as 'After_cooler_heat_rejection'
,[Main motor F class IP 55 kW] as 'Main_motor_F_class_IP_55_kW'
,[Main motor max power]as 'Main_motor_max_power'
,[Main motor protection details]as 'Main_motor_protection_details'
,[Speed of rotation rpm]as 'Speed_of_rotation_rpm'
,cast([Fan motor kW EA] as nvarchar(200))  + case when ([Fan motor kW EA]>0 or [Fan motor kW EW]>0) then ' / ' else '' end  + cast([Fan motor kW EW] as nvarchar(200)) as 'Fan_motor_Ea_EW'
,[Speed of rotation rpm fan motor]as 'Speed_of_rotation_rpm_fan_motor'
,[Voltage tolerance]as 'Voltage_tolerance'
,[Fuse max 1 J�nnite A]as 'Fuse_max_1_J�nnite_A'
,[Fuse max 2 J�nnite A]as 'Fuse_max_2_J�nnite_A'
,Round((((isnull([Fan motor kW EW],0) + (Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/((SQRT(3))* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package440'
,Round((((isnull([Fan motor kW EW],0) + (Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)))*1000)*1.07)/((SQRT(3))* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),0) as 'Current_for_package640'
,[Starting current Ia In DOL YD SOFT START]as 'Starting_current_Ia_In_DOL_YD_SOFT_START'
,[Control voltage V]as 'Control_voltage_V'
,[Oil quantity I]as 'Oil_quantity_I'
,[Oil content mg m]as 'Oil_content_mg_m'
,[Air outlet T1]as 'Air_outlet_T1'
,[Air outlet T1 water cooled]as 'Air_outlet_T1_water_cooled'
,[Condensate drain T4]as 'Condensate_drain_T4'
,[Weight with NOVOX canopy]as 'Weight_with_NOVOX_canopy'
,[General arrangement drawing with NOVOX canopy EA]as 'General_arrangement_drawing_with_NOVOX_canopy_EA'
,[General arrangement drawing with NOVOX canopy EW]as 'General_arrangement_drawing_with_NOVOX_canopy_EW'
,[Cable gland Power]as 'Cable_gland_Power'
,[Cable gland Alarm Signal]as 'Cable_gland_Alarm_Signal'
,[Pressure Level LpA with NOVOX canopy EA]as 'Pressure_Level_LpA_with_NOVOX_canopy_EA'
,[Pressure Level LpA with NOVOX canopy EW]as 'Pressure_Level_LpA with_NOVOX_canopy_EW'
,[Datasheet revision] AS 'Datasheet revision'
,[Maximum working pressure barg] AS 'Maximum_working_ressure_barg_HEADER'
,[Maximum ambient temperature C] as 'Maximum_ambient_temperature_C'
,case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus'
,Lis�tietoja AS 'Extra_notes'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2'
,ROUND(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) * 0.95),2) as 'Heat_Dissipation_EA'
,ROUND((((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1)) *100/[Motor efficiency])*((100-[Motor efficiency])/100)) +(((Round(Convert(float,replace([Specific power consumption kW m min],',','') )*((((Convert(float,replace([Air end volume per revolution],',','') ) *(Convert(float,replace([Speed of rotation rpm],',','') )))*[Motor pulley]/[Air end pulley])*(convert(float,replace([Volumetric efficiency],',','') )/100))/1000)*0.99,1))*100/[Motor efficiency])*0.02),2) as 'Heat_Dissipation_EW'
,[Main motor temperature rise] AS 'Main_motor_temperature_rise'
 FROM  dbo.Data 
  dt
 left  join [Aftercooler power correction factor] lt on lt.powercorrection = dt.[Maximum working pressure barg] 
    WHERE (CODE ='TMC54-85')
	--AND [Capacity at normal working pressure m min] IS   NULL 
