USE [MarineData]
GO

/****** Object:  View [dbo].[RPT_GetReportData_isot_IE3]    Script Date: 6/2/2021 5:10:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--/****** Object:  View [dbo].[RPT_GetReportData_isot_IE3]    Script Date: 3/8/2021 3:03:00 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

ALTER VIEW [dbo].[RPT_GetReportData_isot_IE3]
AS
SELECT Laskuri, [Datasivun numero] , [Model_No] + ' - ' + Cast([Maximum working pressure barg] as nvarchar(50)) +' ' + Cooling as 'Maximum working pressure barg',
Convert(varchar,[Valid from],101) as 'Valid from' , [1 Voltage] +' V / ' + [1 Frequency] + ' Hz' +' ' + [1 Starting type] as '1Voltage'
, [2 Voltage] +' V / ' + [2 Frequency] + ' Hz' +' ' + [2 Starting type] as '2Voltage',[Normal working pressure barg] as 'Normal working pressure barg',
[Capacity at normal working pressure m min] * 60 as 'Capacity at normal working pressure m min calculated' ,[Capacity at normal working pressure m min] as 'Capacity at normal working pressure m min'
,[Shaft power at normal working pressure kW] as 'Shaft power at normal working pressure kW'
,Cast([Maximum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Maximum working pressure barg]/10 as nvarchar(20)) as 'Maximum working pressure barg(mPag)'
,Cast([Minimum working pressure barg] as nvarchar(20))  +Cast(' / ' as nvarchar(20)) + cast([Minimum working pressure barg]/10 as nvarchar(20)) as 'Minimum working pressure barg(mPag)'
,round([Shaft power at normal working pressure kW]*0.22,1) as 'Shaft power at normal working pressure kW calculated'
,[Transmission Gear i]  as 'Tranmission', [Speed of rotation rpm],
case when Transmission ='B'THEN (CEILING(([Speed of rotation rpm] *[Motor pulley])/[Air end pulley]))
WHEN Transmission='D'THEN [Speed of rotation rpm]
WHEN Transmission='G' THEN (CEILING([Speed of rotation rpm] * [Transmission Gear i]))
ELSE '' END AS 'Transmissioncalculated',
[Motor pulley],[Air end pulley],
case when Transmission ='B'THEN 'Belt'
WHEN Transmission='D'THEN 'Direct'
WHEN Transmission='G' THEN 'Gear, i ='
ELSE '' END AS 'TransmissionStatus',cast('0' as nvarchar(20))+cast(' - ' as nvarchar(12)) +cast([Maximum ambient temperature C] as nvarchar(12)) as 'Allowed ambient temperature MIN MAX C'
,case when [Compressed air temperature above cooling medium temperature C] >6 then '6 - '+ cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12)) else '' + cast([Compressed air temperature above cooling medium temperature C] as nvarchar(12)) end as 'Compressed air temperature above cooling medium temperature C'
,[Cooling air flow m s] as 'Cooling air flow m s',[Maximum cooling air pressure drop Pa] as 'Maximum cooling air pressure drop Pa'
,[Water in C Fresh water cooled] as 'Water in C Fresh water cooled'
,[Water out C Fresh water cooled] as 'Water out C Fresh water cooled'
,[Water in C Sea water cooled] as 'Water in C Sea water cooled'
,[Water out C Sea water cooled] as 'Water  Out C Sea water cooled'
,cast([Minimum water inlet pressure with zero back bar MPa Serial] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero back bar MPa Serial]/10) as nvarchar(12)) as 'Minimum water inlet pressure with zero ba bar MPa Parallel' 
,cast([Minimum water inlet pressure with zero ba bar MPa Parallel] as nvarchar(30)) +' / ' +cast(([Minimum water inlet pressure with zero ba bar MPa Parallel]/10) as nvarchar(12))  as 'Minimum water inlet pressure with zero back bar MPa After'
,[Maximum inlet pressure barg] as 'Maximum inlet pressure barg'
,[Water in out T2 T3] as 'Water in out T2 T3'
,[Main motor F class IP 55 kW] as 'Main motor F class IP 55 kW'
,cast([Fan motor kW EA] as nvarchar(max))  +cast(' / ' as nvarchar(10)) + cast([Fan motor kW EW] as nvarchar(max)) as 'Fan motor kW EA / EW'
,[Speed of rotation rpm fan motor] as 'Speed of rotation rpm fan motor'
,[Motor cos fii]
,[Motor efficiency]
,[Maximum working pressure barg] as 'Maximum working pressure barg single row'
,[Shaft power at normal working pressure kW] *0.75 as 'Oil_cooler_heat_rejection'
,(Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       (4.2 *( [water out c fresh water cooled] - [water in c fresh water cooled] ) ) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) 
       , 2))  AS 'Water_flow_Fresh_water_cooled_60_45' 
	   ,(Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       (4.2 *( [Water out C Sea water cooled] - [Water in C Sea water cooled] ) ) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) 
       , 2))  AS 'Water_flow_Fresh_water_cooled_60_45_sea' 
	   ,[Extra factor for water flow at 60 humidity fresh] 
	   ,(Round(((( ( 60 
       - [compressed air temperature above cooling medium temperature c] ) 
         * [capacity at normal working pressure m min] * 1.2 
             ) / 60 )+ ((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*lt.Weighting),0))
               AS 'After_cooler_heat_rejection_60_45' 
			   ,ROUND([Shaft power at normal working pressure kW]*0.08,1) AS 'HEAT_DISSSIPATION'
 ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / ( 
       1.2 * 
       [cooling air flow m s] ), 0) AS 'Cooling air temperature rise (C�)' 
	   ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       ( 4.2 * ( [water out c fresh water cooled] 
                 - [water in c fresh water cooled] ) ) 
       , 2) AS 'Water_flow_Fresh water cooled' 
	   ,Round( 
       ( ( 0.75 * [shaft power at normal working pressure kw] ) + ( ( 60 
               - [compressed air temperature above cooling medium temperature c] 
               ) * 
                 [capacity at normal working pressure m min] * 1.2 / 60 ) ) / 
       ( 4.2 * ( [water out c sea water cooled] - [water in c sea water cooled] 
               ) ), 2) 
       AS 'Water_flow_Sea water cooled' 
	   
	   ,round(( ( 60 
       - [compressed air temperature above cooling medium temperature c] ) 
         * 
                 [capacity at normal working pressure m min] * 1.2 
             ) / 60 
              ,0) + Round((((0.04* [Capacity at normal working pressure m min]*1.1)/60)*2260 *lt.Weighting ),0)AS 'After_cooler_heat_rejection' 
			  ,Round( (([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02)),0) AS 
       'Heat dissipation_EW'
	   ,CAST([Minimum water inlet pressure with zero back Serial sea] AS NVARCHAR(12)) +' / ' + CAST([Minimum water inlet pressure with zero back Serial sea] /10 AS NVARCHAR(12))   as 'Minimum_water_inlet_pressure_with_zero_back_Serial_sea'
	   ,[Main motor F class IP 55 kW] as 'Main_motor_F_class'
	   ,[Main motor protection details]
	   ,[Main motor temperature rise]
	   
	   ,[Voltage tolerance]
	   ,[Fuse max 1 J�nnite A]
,[Fuse max 2 J�nnite A]
 ,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package440'
 ,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package640'
 ,[Starting current Ia In DOL YD SOFT START]
 ,[Control voltage V]
,[Oil quantity I]
,[Oil content mg m]
,[Oil quantity EA I]
,[Air outlet T1]
,[Condensate drain T4]
,[Weight without canopy]
,[Weight with NOVOX canopy]
,[General arrangement drawing SW]
,[General arrangement drawing FW]
,[General arrangement drawing with NOVOX canopy EW]
,[Cable gland Power]
,[Cable gland Alarm Signal]
,[Pressure Level LpA with NOVOX canopy EW]
,[Pressure Level LpA without canopy EW]
,Lis�tietoja as 'Extra notes'
,[Datasheet revision]
,Round((([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100)+(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02))/((1.25*10)),1) as 'Cooling_air_flow_for_ventilation_of_compressor_room'
,[1 Voltage] + ' V ' as  'voltage_1'
,[2 Voltage] + ' V ' as'Voltage_2',
[Maximum ambient temperature C] as 'Maximum ambient temperature C',
[Air outlet T1 water cooled] as 'Air outlet T1 water cooled'
 FROM  dbo.Data dt
 left  join [Aftercooler power correction factor] lt on cast(lt.powercorrection as float)= cast(dt.[Maximum working pressure barg] as nvarchar(122))
 
 --where dt.[Datasivun numero]='MDATA3646'
where 
CODE in('TMC105-235', 'TMC105-235SL' ,'TMC240-365','ULM90-200','TMC245-360','TMC400-450')  and Cooling='EW'  
and convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','') ) >0 
and  [Main motor temperature rise] is not null  
--and [Datasheet status]='In Use'
--nd [Datasivun numero]='MDATA3970'




GO


