//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MarineDataSheet
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_GetReportData_TMC_49_84_datasivu
    {
        public int Laskuri { get; set; }
        public string Datasivun_numero { get; set; }
        public string Maximum_working_pressure_barg_model { get; set; }
        public string Valid_from { get; set; }
        public string C1Voltage { get; set; }
        public string C2Voltage { get; set; }
        public Nullable<double> Maximum_working_pressure_barg_Header { get; set; }
        public string Normal_working_pressure_barg { get; set; }
        public Nullable<double> Capacity_at_normal_working_pressure { get; set; }
        public Nullable<double> Capacity_at_normal_working_pressure_min { get; set; }
        public Nullable<double> Shaft_power_at_normal_working_pressure { get; set; }
        public string Maximum_working_pressure { get; set; }
        public string Minimum_working_pressure { get; set; }
        public Nullable<double> Idling_shaft_power_consumption { get; set; }
        public Nullable<double> Male_rotor_speed_min_max { get; set; }
        public string TransmissionStatus { get; set; }
        public string Datasheet_revision { get; set; }
        public Nullable<decimal> Transmission { get; set; }
        public string Maximum_ambient_temperature { get; set; }
        public string Compressed_air_temperature_above_cooling_medium_temperature { get; set; }
        public Nullable<double> Maximum_cooling_air_pressure_drop_Pa { get; set; }
        public Nullable<double> cooling_air_temperature_rise { get; set; }
        public Nullable<double> waterFlow_oilcooler_60_60 { get; set; }
        public string Water_in_water_out_Fresh_water_cooled { get; set; }
        public string Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial { get; set; }
        public Nullable<double> waterFlow_oilcooler_sea_40_60 { get; set; }
        public string Water_in_water_out_Fresh_water_cooled_sea { get; set; }
        public string Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Parallel { get; set; }
        public Nullable<double> Maximum_inlet_pressure_barg { get; set; }
        public string water_in_out_t2_t4 { get; set; }
        public Nullable<double> oil_cooler_heat_rejection { get; set; }
        public Nullable<double> After_cooler_heat_rejection { get; set; }
        public Nullable<double> Heat_dissipation_Ea { get; set; }
        public Nullable<double> Heat_dissipation_Ew { get; set; }
        public Nullable<double> Main_motor_class_IP_55 { get; set; }
        public string Main_motor_protection_details { get; set; }
        public Nullable<double> Main_motor_max_power { get; set; }
        public Nullable<double> Speed_of_rotation_rpm { get; set; }
        public Nullable<decimal> Fan_motor_kW_EA { get; set; }
        public string Speed_of_rotation_rpm_fan_motor { get; set; }
        public string Voltage_tolerance { get; set; }
        public string Fuse_max_1_Jännite_A { get; set; }
        public string Fuse_max_2_Jännite_A { get; set; }
        public string Starting_current_Ia_In_DOL_YD_SOFT_START { get; set; }
        public string Control_voltage_V { get; set; }
        public Nullable<double> Cooling_air_flow_for_entilation_of_compressor_room_1 { get; set; }
        public Nullable<double> Cooling_air_flow_for_entilation_of_compressor_room_2 { get; set; }
        public string Oil_quantity_I { get; set; }
        public Nullable<double> Oil_content_mg_m { get; set; }
        public string Air_outlet_T1 { get; set; }
        public string Air_outlet_T1_water { get; set; }
        public string Weight_with_canopy_EA_EW { get; set; }
        public string Condensate_drain_T4 { get; set; }
        public string Pressure_Level_LpA_without_canopy_EW { get; set; }
        public string Pressure_Level_LpA_with_NOVOX_canopy_EW { get; set; }
        public string Pressure_Level_LpA_with_NOVOX_canopy_EA { get; set; }
        public string Weight_without_canopy { get; set; }
        public string Weight_with_NOVOX_canopy { get; set; }
        public string General_arrangement_drawing_SW { get; set; }
        public string General_arrangement_drawing_FW { get; set; }
        public string General_arrangement_drawing_with_NOVOX_canopy_EA_EW { get; set; }
        public string General_arrangement_drawing_with_NOVOX_canopy_EA { get; set; }
        public string Cable_gland_Power { get; set; }
        public string Cable_gland_Alarm_Signal { get; set; }
        public string Extra_notes { get; set; }
        public Nullable<double> Maximum_working_ressure_barg_HEADER { get; set; }
        public Nullable<double> Maximum_ambient_temperature_C { get; set; }
        public string Datasheet_revision1 { get; set; }
        public Nullable<double> Current_for_package_EA { get; set; }
        public Nullable<double> Current_for_package_EA_1 { get; set; }
        public Nullable<double> Current_for_package_EW { get; set; }
        public Nullable<double> Current_for_package_Ew_1 { get; set; }
        public string voltage_1 { get; set; }
        public string Voltage_2 { get; set; }
    }
}
