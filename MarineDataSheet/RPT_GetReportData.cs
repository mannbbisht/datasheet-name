//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MarineDataSheet
{
    using System;
    using System.Collections.Generic;
    
    public partial class RPT_GetReportData
    {
        public int Laskuri { get; set; }
        public string Datasivun_numero { get; set; }
        public string Maximum_working_pressure_barg { get; set; }
        public string Valid_from { get; set; }
        public string C1Voltage { get; set; }
        public string C2Voltage { get; set; }
        public string Normal_working_pressure_barg { get; set; }
        public Nullable<double> Capacity_at_normal_working_pressure_m_min_calculated { get; set; }
        public Nullable<double> Capacity_at_normal_working_pressure_m_min { get; set; }
        public Nullable<double> Shaft_power_at_normal_working_pressure_kW { get; set; }
        public string Maximum_working_pressure_barg_mPag_ { get; set; }
        public string Minimum_working_pressure_barg_mPag_ { get; set; }
        public Nullable<double> Shaft_power_at_normal_working_pressure_kW_calculated { get; set; }
        public string Tranmission { get; set; }
        public Nullable<double> Speed_of_rotation_rpm { get; set; }
        public Nullable<double> Transmissioncalculated { get; set; }
        public Nullable<int> Motor_pulley { get; set; }
        public Nullable<int> Air_end_pulley { get; set; }
        public string TransmissionStatus { get; set; }
        public string Allowed_ambient_temperature_MIN_MAX_C { get; set; }
        public string Compressed_air_temperature_above_cooling_medium_temperature_C { get; set; }
        public Nullable<double> Cooling_air_flow_m_s { get; set; }
        public Nullable<double> Maximum_cooling_air_pressure_drop_Pa { get; set; }
        public Nullable<double> Water_in_C_Fresh_water_cooled { get; set; }
        public Nullable<double> Water_out_C_Fresh_water_cooled { get; set; }
        public Nullable<double> Water_in_C_Sea_water_cooled { get; set; }
        public Nullable<double> Water__Out_C_Sea_water_cooled { get; set; }
        public string Minimum_water_inlet_pressure_with_zero_ba_bar_MPa_Parallel { get; set; }
        public Nullable<decimal> Minimum_water_inlet_pressure_with_zero_back_bar_MPa_After { get; set; }
        public Nullable<double> Maximum_inlet_pressure_barg { get; set; }
        public string Water_in_out_T2_T3 { get; set; }
        public Nullable<double> Main_motor_F_class_IP_55_kW { get; set; }
        public string Fan_motor_kW_EA___EW { get; set; }
        public string Speed_of_rotation_rpm_fan_motor { get; set; }
        public string Fuse_max_1_Jännite_A { get; set; }
        public string Fuse_max_2_Jännite_A { get; set; }
        public Nullable<double> Motor_cos_fii { get; set; }
        public Nullable<double> Motor_efficiency { get; set; }
        public string Starting_current_Ia_In_DOL_YD_SOFT_START { get; set; }
        public string Control_voltage_V { get; set; }
        public Nullable<double> Oil_quantity_I { get; set; }
        public Nullable<double> Oil_content_mg_m { get; set; }
        public Nullable<double> Oil_quantity_EA_I { get; set; }
        public string Air_outlet_T1 { get; set; }
        public string Condensate_drain_T4 { get; set; }
        public string Pressure_Level_LpA_with_NOVOX_canopy_EA { get; set; }
        public string Pressure_Level_LpA_with_NOVOX_canopy_EW { get; set; }
        public string Pressure_Level_LpA_without_canopy_EA { get; set; }
        public string Pressure_Level_LpA_without_canopy_EW { get; set; }
        public string Weight_without_canopy { get; set; }
        public string Weight_with_NOVOX_canopy { get; set; }
        public string Weight_with_NOVOX_canopy_EA { get; set; }
        public string General_arrangement_drawing_EA { get; set; }
        public string General_arrangement_drawing_FW { get; set; }
        public string General_arrangement_drawing_SW { get; set; }
        public string General_arrangement_drawing_with_NOVOX_canopy_EA { get; set; }
        public string General_arrangement_drawing_with_NOVOX_canopy_EW { get; set; }
        public string Cable_gland_Power { get; set; }
        public string Cable_gland_Alarm_Signal { get; set; }
        public string Extra_notes { get; set; }
        public string Datasheet_revision { get; set; }
        public Nullable<double> Maximum_working_pressure_barg_single_row { get; set; }
        public string Heat_dissipation { get; set; }
        public Nullable<double> HW_MaximumamidentTemperature { get; set; }
        public Nullable<double> HYD_SHAFTPOWER { get; set; }
        public Nullable<decimal> HYDTransmissioncalculated { get; set; }
        public string HYD_COMPRESSEDAIRTEMPERATURE { get; set; }
        public string HYD_WATERINCFRESHWATERCOOLED { get; set; }
        public string HYD_INLETPRESSUREZEROBACK { get; set; }
        public string HYD_inlet_pressure_with_zero_back_Serial_sea { get; set; }
        public Nullable<double> HYD_OILCOOLERREJECTED { get; set; }
        public Nullable<double> HYD_HEATDISSIPATION { get; set; }
        public string HYD_MAINMOTOR { get; set; }
        public string Hydraulic_motor_speed_rotation_max_min { get; set; }
        public string Hydraulic_oil_max_min { get; set; }
        public string Hydraulic_oil_pressure_bar_max_min { get; set; }
        public string Air_outlet_T1_water_cooled { get; set; }
        public string C2_Voltage { get; set; }
        public Nullable<double> HYD_Water_flow_Fresh_water_cooled_60_45 { get; set; }
        public Nullable<double> HYD_Water_flow_Fresh_water_cooled_60_45_sea { get; set; }
        public Nullable<decimal> Extra_factor_for_water_flow_at_60_humidity_fresh { get; set; }
        public Nullable<double> HYD_After_cooler_heat_rejection_60_45 { get; set; }
        public Nullable<double> HYD_HEAT_DISSSIPATION { get; set; }
        public Nullable<double> Cooling_air_temperature_rise__C__ { get; set; }
        public Nullable<double> Water_flow__l_s___Fresh_water_cooled { get; set; }
        public Nullable<double> Water_flow__l_s___Sea_water_cooled { get; set; }
        public Nullable<double> Oil_cooler_heat_rejection__kW_ { get; set; }
        public Nullable<double> After_cooler_heat_rejection__kW_ { get; set; }
        public string Heat_dissipation__kW__EA___EW { get; set; }
        public Nullable<double> Current_for_package440 { get; set; }
        public Nullable<double> Current_for_package660 { get; set; }
        public string voltage_1 { get; set; }
        public string Voltage_2 { get; set; }
        public string model_no { get; set; }
        public Nullable<decimal> Transmission_gear_1 { get; set; }
    }
}
