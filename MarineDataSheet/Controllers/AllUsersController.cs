﻿using MarineDataSheet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;

namespace MarineDataSheet.Controllers
{
    public class AllUsersController : Controller
    {
        // GET: AllUsers
        public ActionResult Users()
        {
            return View();
        }

        public ActionResult LoadUsersData()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;

                var rolesData = ShowallUsers(sortColumn, sortColumnDir, searchValue);
                recordsTotal = rolesData.Count();
                var data = rolesData.Skip(skip).Take(pageSize).ToList();

                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IQueryable<RegistrationViewSummaryModel> ShowallUsers(string sortColumn, string sortColumnDir, string Search)
        {
            var _context = new MarineDataEntities();

            var IQueryabletimesheet = (from registration in _context.Registrations
                                       join AssignedRoles in _context.AssignedRoles on registration.RegistrationID equals AssignedRoles.RegistrationID
                                       join AssignedRolesAdmin in _context.Registrations on AssignedRoles.AssignToAdmin equals AssignedRolesAdmin.RegistrationID
                                       where registration.RoleID == 2
                                       select new RegistrationViewSummaryModel
                                       {
                                           Name = registration.Name,
                                           AssignToAdmin = string.IsNullOrEmpty(AssignedRolesAdmin.Name) ? "*Not Assigned*" : AssignedRolesAdmin.Name.ToUpper(),
                                           RegistrationID = registration.RegistrationID,
                                           EmailID = registration.EmailID,
                                           Mobileno = registration.Mobileno,
                                           Username = registration.Username
                                       });

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                IQueryabletimesheet = IQueryabletimesheet.OrderBy(sortColumn + " " + sortColumnDir);
            }
            if (!string.IsNullOrEmpty(Search))
            {
                IQueryabletimesheet = IQueryabletimesheet.Where(m => m.Name == Search);
            }

            return IQueryabletimesheet;

        }

        public ActionResult UserDetails(int? RegistrationID)
        {
            var _context = new MarineDataEntities();
            try
            {
                if (RegistrationID == null)
                {

                }
                var userDetailsResponse = from s in _context.Registrations where s.RoleID == 2 && s.RegistrationID == RegistrationID select s;
                return PartialView("_UserDetails", userDetailsResponse.FirstOrDefault());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult Admin()
        {
            return View();
        }


        public ActionResult LoadAdminsData()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;

                var rolesData = ShowallAdmin(sortColumn, sortColumnDir, searchValue);
                recordsTotal = rolesData.Count();
                var data = rolesData.Skip(skip).Take(pageSize).ToList();

                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
        public IQueryable<RegistrationViewSummaryModel> ShowallAdmin(string sortColumn, string sortColumnDir, string Search)
        {
            var _context = new MarineDataEntities();

            var IQueryabletimesheet = (from registration in _context.Registrations
                                       where registration.RoleID == 1
                                       select new RegistrationViewSummaryModel
                                       {
                                           Name = registration.Name,
                                           RegistrationID = registration.RegistrationID,
                                           EmailID = registration.EmailID,
                                           Mobileno = registration.Mobileno,
                                           Username = registration.Username
                                       });

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                IQueryabletimesheet = IQueryabletimesheet.OrderBy(sortColumn + " " + sortColumnDir);
            }
            if (!string.IsNullOrEmpty(Search))
            {
                IQueryabletimesheet = IQueryabletimesheet.Where(m => m.Name == Search);
            }

            return IQueryabletimesheet;

        }

        public ActionResult AdminDetails(int? RegistrationID)
        {
            var _context = new MarineDataEntities();
            try
            {
                if (RegistrationID == null)
                {

                }
                var userDetailsResponse = from s in _context.Registrations where s.RoleID == 1 && s.RegistrationID == RegistrationID select s;
                return PartialView("_UserDetails", userDetailsResponse.FirstOrDefault());
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}