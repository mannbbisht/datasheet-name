﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using log4net;
using MarineDataSheet.Helpers;
using Rotativa.MVC;

namespace MarineDataSheet.Controllers
{
    public class ViewDataSheetController : Controller
    {
        MarineDataEntities md = new MarineDataEntities();
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(DataController));
        // GET: ViewDataSheet
        public ActionResult Index()
        {
            log.Info("---Action method in Data Controller was called--");

            return View();
        }
        [HttpPost]
        public ActionResult LoadData()
        {
            //Get parameters
            try
            {
                log.Info("Before Paramater called");

                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var Datasivun_numero = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
                var Revision = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
                var Code = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
                var Model = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
                var Barg = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
                var notes = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
                var Enigneering_notes = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
                //Get Sort columns value
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;
                log.Info("ActionMethod was Called Before Database");
                using (MarineDataEntities dc = new MarineDataEntities())
                {
                    var dv = (from a in dc.Data where (a.Datasheet_status == "In Use") select a);
                    //Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        dv = dv.OrderBy(sortColumn + " " + sortColumnDir);

                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        dv = dv.Where(m => m.Datasivun_numero.Contains(searchValue) || m.Code.Contains(searchValue) || m.Model_No.Contains(searchValue));
                    }
                    else if (!string.IsNullOrEmpty(Datasivun_numero))
                    {
                        dv = dv.Where(m => m.Datasivun_numero.Contains(Datasivun_numero));
                    }
                    else if (!string.IsNullOrEmpty(Revision))
                    {
                        dv = dv.Where(m => m.Datasheet_revision.Contains(Revision));
                    }
                    else if (!string.IsNullOrEmpty(Code))
                    {
                        dv = dv.Where(m => m.Code.Contains(Code));
                    }
                    else if (!string.IsNullOrEmpty(Model))
                    {
                        dv = dv.Where(m => m.Model_No.Contains(Model));
                    }
                    else if (!string.IsNullOrEmpty(Barg))
                    {
                        dv = dv.Where(m => m.Maximum_working_pressure_barg.ToString().Contains(Barg));
                    }
                    else if (!string.IsNullOrEmpty(notes))
                    {
                        dv = dv.Where(m => m.Lisätietoja.ToString().Contains(notes));
                    }
                    else if (!string.IsNullOrEmpty(Enigneering_notes))
                    {
                        dv = dv.Where(m => m.Suunnittelun_kommentit.ToString().Contains(Enigneering_notes));
                    }
                    totalRecords = dv.Count();
                    var data = dv.Skip(skip).Take(pageSize).ToList();
                    return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString());
                throw ex;
            }
            // get Start (paging start index) and length (page size for paging)

        }
    }
}