﻿using MarineDataSheet.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using log4net;
namespace MarineDataSheet.Controllers
{
    public class ViewRevisionController : Controller
    {
        MarineDataEntities dc = new MarineDataEntities();
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(DataController));
        // GET: ViewRevision
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadData()
        {
            //Get parameters
            try
            {
                log.Info("Before Paramater called");

                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var Datasivun_numero = string.Empty;
                var Code = string.Empty;
                var Model = string.Empty;
                var voltage_1 = string.Empty;
                var frequency_1 = string.Empty;
                var Starting_1 = string.Empty;
                var Voltage_2 = string.Empty;
                var frequency_2 = string.Empty;
                var Starting_2 = string.Empty;
                var Barg = string.Empty;
                var Cooling = string.Empty;
                var AmbientTemperature = string.Empty;
                var Yield = string.Empty;

                var datasheet_type = Convert.ToString(Session["datasheettype"]);

                if (Convert.ToString(Session["Laskuri"]) != "")
                {
                    Datasivun_numero = Convert.ToString(Session["Datasivun_numero"]);
                    Code = Convert.ToString(Session["Code"]);
                    Model = Convert.ToString(Session["Model"]);
                    voltage_1 = Convert.ToString(Session["voltage_1"]);
                    frequency_1 = Convert.ToString(Session["frequency_1"]);
                    Starting_1 = Convert.ToString(Session["Starting_1"]);
                    Voltage_2 = Convert.ToString(Session["Voltage_2"]);
                    frequency_2 = Convert.ToString(Session["frequency_2"]);
                    Starting_2 = Convert.ToString(Session["Starting_2"]);
                    Barg = Convert.ToString(Session["Barg"]);
                    Cooling = Convert.ToString(Session["Cooling"]);
                    AmbientTemperature = Convert.ToString(Session["AmbientTemperature"]);
                    Yield = Convert.ToString(Session["Yield"]);


                }
                else
                {
                    Datasivun_numero = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();
                    Code = Request.Form.GetValues("columns[2][search][value]").FirstOrDefault();
                    Model = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();
                    voltage_1 = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
                    frequency_1 = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
                    Starting_1 = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
                    Voltage_2 = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
                    frequency_2 = Request.Form.GetValues("columns[8][search][value]").FirstOrDefault();
                    Starting_2 = Request.Form.GetValues("columns[9][search][value]").FirstOrDefault();
                    Barg = Request.Form.GetValues("columns[10][search][value]").FirstOrDefault();
                    Cooling = Request.Form.GetValues("columns[11][search][value]").FirstOrDefault();
                    AmbientTemperature = Request.Form.GetValues("columns[12][search][value]").FirstOrDefault();
                    Yield = Request.Form.GetValues("columns[13][search][value]").FirstOrDefault().ToString();

                    Session["Model"] = Model;
                    Session["Code"] = Code;
                    Session["voltage_1"] = voltage_1;
                    Session["frequency_1"] = frequency_1;
                    Session["Starting_1"] = Starting_1;
                    Session["Voltage_2"] = Voltage_2;
                    Session["frequency_2"] = frequency_2;
                    Session["Starting_2"] = Starting_2;
                    Session["Barg"] = Barg;
                    Session["Cooling"] = Cooling;
                    Session["AmbientTemperature"] = AmbientTemperature;
                    Session["Yield"] = Yield;
                    Session["Datasivun_numero"] = Datasivun_numero;

                }
                //Get Sort columns value
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int totalRecords = 0;
                log.Info("ActionMethod was Called Before Database");
                using (MarineDataEntities dc = new MarineDataEntities())
                {
                    var dv = (from a in dc.Data where (a.Datasheet_status == "NotIn Use") select a);

                    //Sorting
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        dv = dv.OrderBy(sortColumn + " " + "desc");

                    }
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        dv = dv.Where(m => m.Datasivun_numero.Contains(searchValue) || m.Code.Contains(searchValue) || m.Model_No.Contains(searchValue));
                    }
                    if (!string.IsNullOrEmpty(Datasivun_numero))
                    {
                        dv = dv.Where(m => m.Datasivun_numero.Contains(Datasivun_numero));
                    }
                    if (!string.IsNullOrEmpty(Code))
                    {
                        dv = dv.Where(m => m.Code.Equals(Code) || m.Code.Contains(Code));
                    }
                    if (!string.IsNullOrEmpty(Model))
                    {
                        Session[Model] = Model;
                        dv = dv.Where(m => m.Model_No.Contains(Model) || m.Model_No.Equals(Model));
                    }
                    if (!string.IsNullOrEmpty(voltage_1))
                    {
                        dv = dv.Where(m => m.C1_Voltage.Equals(voltage_1) || m.C1_Voltage.Contains(voltage_1));
                    }
                    if (!string.IsNullOrEmpty(frequency_1))
                    {
                        dv = dv.Where(m => m.C1_Frequency.Equals(frequency_1) || m.C1_Frequency.Contains(frequency_1));
                    }
                    if (!string.IsNullOrEmpty(Starting_1))
                    {
                        dv = dv.Where(m => m.C1_Starting_type.Equals(Starting_1) || m.C1_Starting_type.Contains(Starting_1));
                    }
                    if (!string.IsNullOrEmpty(Voltage_2))
                    {
                        dv = dv.Where(m => m.C2_Voltage.Equals(Voltage_2) || m.C2_Voltage.Contains(Voltage_2));
                    }
                    if (!string.IsNullOrEmpty(frequency_2))
                    {
                        dv = dv.Where(m => m.C2_Frequency.Equals(frequency_2) || m.C2_Frequency.Contains(frequency_2));
                    }
                    if (!string.IsNullOrEmpty(Starting_2))
                    {
                        dv = dv.Where(m => m.C2_Starting_type.Equals(Starting_2) || m.C2_Starting_type.Contains(Starting_2));
                    }
                    if (!string.IsNullOrEmpty(Barg))
                    {
                        dv = dv.Where(m => m.Maximum_working_pressure_barg.ToString().Contains(Barg) || m.Maximum_working_pressure_barg.ToString().Equals(Barg));
                    }
                    if (!string.IsNullOrEmpty(Cooling))
                    {
                        dv = dv.Where(m => m.Cooling.Equals(Cooling) || m.Cooling.Contains(Cooling));
                    }
                    if (!string.IsNullOrEmpty(AmbientTemperature))
                    {
                        dv = dv.Where(m => m.Maximum_ambient_temperature_C.ToString().Contains(AmbientTemperature) || m.Maximum_ambient_temperature_C.ToString().Equals(AmbientTemperature));
                    }
                    if (!string.IsNullOrEmpty(Yield))
                    {
                        dv = dv.Where(m => m.Capacity_at_normal_working_pressure_m_min.ToString().Contains(Yield) || m.Capacity_at_normal_working_pressure_m_min.ToString().Equals(Yield));
                    }
                    if (Session["Laskuri"] != null)
                    {
                        string type = Convert.ToString(Session["datasheettype"]);
                        dv = dv.Where(x => x.Model_No.Contains(Model) ||
                        x.Code.Contains(Code) ||
                        x.C1_Voltage.Contains(voltage_1) ||
                        x.C1_Frequency.Contains(frequency_1) ||
                        x.C1_Starting_type.Contains(Starting_1) ||
                        x.C2_Voltage.Contains(Voltage_2) ||
                        x.C2_Frequency.Contains(frequency_2) ||
                        x.C2_Starting_type.Contains(Starting_2) ||
                        x.Maximum_working_pressure_barg.ToString().Contains(Barg) ||
                        x.Cooling.Contains(Cooling) ||
                        x.Maximum_ambient_temperature_C.ToString().Contains(AmbientTemperature) ||
                        x.Capacity_at_normal_working_pressure_m_min.ToString().Contains(Yield) ||
                        x.Datasivun_numero.Contains(Datasivun_numero)
                        //x.Datasivun_numero.Equals(type)

                        );
                    }
                    totalRecords = dv.Count();
                    var data = dv.Skip(skip).Take(pageSize).ToList();
                    Session["Laskuri"] = null;
                    return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.Message.ToString());
                throw ex;
            }
            // get Start (paging start index) and length (page size for paging)

        }
        public ActionResult ViewDatasheet(int ID)
        {
            Session["Laskuri"] = ID;



            var userDetailsResponse = dc.Data.Where(x => x.Laskuri == ID);
            var code = userDetailsResponse.Select(x => x.Code ?? "").FirstOrDefault().ToString();
            var Capacity_at_normal_working_pressure_m_min = userDetailsResponse.Select(x => x.Capacity_at_normal_working_pressure_m_min ?? null).FirstOrDefault();
            var Extra_factor_for_water_flow_at_60_humidity_fresh = userDetailsResponse.Select(x => x.Extra_factor_for_water_flow_at_60_humidity_fresh ?? null).FirstOrDefault();
            var Main_motor_F_class_IP_55_kW = userDetailsResponse.Select(x => x.Main_motor_F_class_IP_55_kW ?? null).FirstOrDefault();
            var model = userDetailsResponse.Select(x => x.Model_No ?? null).FirstOrDefault();
            var cooling = userDetailsResponse.Select(x => x.Cooling ?? null).FirstOrDefault();

            var Datasheettype = userDetailsResponse.Select(x => x.Datasheet_status ?? "").FirstOrDefault().ToString();

            Session["datasheettype"] = Datasheettype.ToString();

            var LatestDatasheetNo = dc.Usp_GetLatesDatasheetNumber().SingleOrDefault();
            ViewBag.Message = LatestDatasheetNo;

            #region "Form Paramaterwise"
            if ((code == "EMH21-44" || code == "TMC54-85" || code == "TMC6-26A") && Convert.ToDouble(Capacity_at_normal_working_pressure_m_min) > 0) //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if (code != "TMC400-450" && Convert.ToDouble(Main_motor_F_class_IP_55_kW) > 87 && cooling == "EA") //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "WD7-22A" || code == "WD21-44" || code == "WD54-85") //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "EMH21-44" && Capacity_at_normal_working_pressure_m_min is null) //Syöttöpohja_UUSI
            {
                return View("Feedbase_new", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC7-27" && model != "TMC 21 SA") //Syöttöpohja_UUSI
            {
                return View("Feedbase_new", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC54-85" && Capacity_at_normal_working_pressure_m_min is null) //Syöttöpohja_UUSI
            {
                return View("Feedbase_new", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Extra_factor_for_water_flow_at_60_humidity_fresh is null) //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_SA_UUSI
            {
                return View("Feedbase_SA_UUSI", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC7-27" && model == "TMC 21 SA") //Syöttöpohja_SA_UUSI
            {
                return View("Feedbase_SA_UUSI", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200") && (cooling == "EW" || cooling == "EA / EW") && Extra_factor_for_water_flow_at_60_humidity_fresh == null) //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC150-235" || code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && (cooling == "EW" || cooling == "EA/EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC150-235" || code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && cooling == "EA / EW v2" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC245-360" && (cooling == "EW" || cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC400-450" && cooling == "EA") //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if (cooling == "HW")//Syöttöpohja_HYD
            {
                return View("Feedbase_HYD", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA") && cooling == "EW" && Extra_factor_for_water_flow_at_60_humidity_fresh is null) //Syöttöpohja_vanha
            {
                return View("Feedbase_old", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && (cooling == "EW" || cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EA / EW v2" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC245-360SA" && (cooling == "EW" || cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0) //Syöttöpohja_isot_UUSI
            {
                return View("Feedbase_big", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC86-124" && model == "TMC 95 SA") //Syöttöpohja_TMC 95 SA
            {
                return View("Feedbase_TMC_95_SA", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "WD7-22B") //Syöttöpohja_UUSI
            {
                return View("Feedbase_new", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "RRM105-185") //Syöttöpohja_RRM
            {
                return View("Feedbase_RRM", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC86-124" && model != "TMC 95 SA") //Syöttöpohja_TMC 86-124
            {
                return View("Feedbase_TMC_86_124", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC49-84" && (model == "TMC 49" || model == "TMC 64" || model == "TMC 84")) //Syöttöpohja_TMC 49-84
            {
                return View("Feedbase_TMC_49_84", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC49-84" && (model == "TMC 55 SA" || model == "TMC 84 SA")) //Syöttöpohja_TMC 55-84 SA
            {
                //return PartialView("_Feedbase_TMC_55_84_SA", userDetailsResponse.FirstOrDefault());
                return View("Feedbase_TMC_55_84_SA", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC35-44" && (model == "TMC 35" || model == "TMC 44")) //Syöttöpohja_UUSI
            {
                return View("Feedbase_new", userDetailsResponse.FirstOrDefault());
            }
            else if (code == "TMC35-44" && (model == "TMC 35 SA" || model == "TMC 44 SA")) //Syöttöpohja_SA_UUSI
            {
                return View("Feedbase_SA_UUSI", userDetailsResponse.FirstOrDefault());
            }
            else
            {
                return View("ViewDatasheet", userDetailsResponse.FirstOrDefault());
            }
            #endregion

        }
    }
}