﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarineDataSheet.Controllers
{
    public class ModifydataController : Controller
    {
        MarineDataEntities md = new MarineDataEntities();
        // GET: Modifydata
        public ActionResult Modifydata(int id)
        {

            var Model = md.Data.Where(m => m.Laskuri == id).FirstOrDefault();
            if (Model != null)
            {
                TempData["LaskuriId"] = id;
                TempData.Keep();
            }
            return View(Model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modifydata(Datum dbmodel)
        {
            try
            {
                
                    md.Entry(dbmodel).State = EntityState.Modified;
                    TempData["MessageModifyDataSheet"] = "DataSheet modified Successfully!";
                    md.SaveChanges();
           
                return RedirectToAction("Index", "Data");
            }
            catch (Exception ex)
            {
                TempData["MessageModifyDataSheet"] = "Error in Modification!";
                throw ex;
            }
            
          
        }

       
    }
}