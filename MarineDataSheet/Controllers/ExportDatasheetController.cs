﻿
using MarineDataSheet.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace MarineDataSheet.Controllers
{
    public class ExportDatasheetController : Controller
    {
        // GET: ExportDatasheet
        MarineDataEntities md = new MarineDataEntities();

       
        public EmptyResult Export(int id)
        {
            string str = GetPrinTDocType(id);
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Datasheet.doc");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-word";
            Response.Output.Write(str);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }
        #region"Report Print Action method"
        public ActionResult ExportPDF(int id)
        {
            var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu(int id)
        {
            var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id).ToList();
            return View(Model);
        }
        public ActionResult Datasivu_HYD(int id)
        {
            var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_IE3(int id)
        {
            var Model = md.RPT_GetReportData_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot(int id)
        {
            var Model = md.RPT_GetReportData_isot.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_IE3(int id)
        {
            var Model = md.RPT_GetReportData_isot_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_IE3_EAEW(int id)
        {
            var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_RRM(int id)
        {
            var Model = md.RPT_GetReportData_isot_RRM.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_RRM_IE3(int id)
        {
            var Model = md.RPT_GetReportData_isot_RRM_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_SA(int id)
        {
            var Model = md.RPT_GetReportData_isot_SA.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_SA_IE3(int id)
        {
            var Model = md.RPT_GetReportData_isot_SA_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_SA_IE3_EAEW(int id)
        {
            var Model = md.RPT_GetReportData_isot_SA_IE3_EAEW.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_SA_UUSI(int id)
        {
            var Model = md.RPT_GetReportData_isot_SA_UUSI.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_UUSI(int id)
        {
            var Model = md.RPT_GetReportData_isot_UUSI.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_isot_UUSI_EA(int id)
        {
            var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_SA(int id)
        {
            var Model = md.RPT_GetReportData_Datasivu_SA.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_SA_IE3(int id)
        {
            var Model = md.RPT_GetReportData_Datasivu_SA_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_SA_UUSI(int id)
        {
            var Model = md.RPT_GetReportData_Datasivu_SA_UUSI.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_UUSI(int id)
        {
            var Model = md.RPT_GetReportData_Datasivu_UUSI.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Datasivu_WD(int id)
        {
            var Model = md.RPT_GetReportData_Datasivu_WD.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult Syöttöpohjan_tulostus(int id)
        {
            var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC21SAdatasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_21_SA_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC21SAdatasivu_IE3(int id)
        {
            var Model = md.RPT_GetReportData_TMC_21_SA_datasivu_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC49_84datasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_49_84_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC54_85datasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC54_85_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC54_85datasivu_IE3(int id)
        {
            var Model = md.RPT_GetReportData_TMC54_85_datasivu_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC55_84SAdatasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_55_84_SA_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC7_27datasivu_IE3(int id)
        {
            var Model = md.RPT_GetReportData_TMC_7_27_datasivu_IE3.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC727datasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_7_27_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC86_124datasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_86_124_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult TMC95SAdatasivu(int id)
        {
            var Model = md.RPT_GetReportData_TMC_95_SA_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        public ActionResult WD722Bdatasivu(int id)
        {
            var Model = md.RPT_GetReportData_WD7_22B_datasivu.Where(m => m.Laskuri == id);
            return View(Model);
        }
        #endregion
        public ActionResult Print(int id)
        {
            MarineDataEntities dc = new MarineDataEntities();
            var userDetailsResponse = dc.Data.Where(x => x.Laskuri == id);
            var code = userDetailsResponse.Select(x => x.Code ?? "").FirstOrDefault().ToString();
            var Capacity_at_normal_working_pressure_m_min = userDetailsResponse.Select(x => x.Capacity_at_normal_working_pressure_m_min ?? null).FirstOrDefault();
            var Extra_factor_for_water_flow_at_60_humidity_fresh = userDetailsResponse.Select(x => x.Extra_factor_for_water_flow_at_60_humidity_fresh ?? null).FirstOrDefault();
            var Main_motor_F_class_IP_55_kW = userDetailsResponse.Select(x => x.Main_motor_F_class_IP_55_kW ?? null).FirstOrDefault();
            var model = userDetailsResponse.Select(x => x.Model_No ?? null).FirstOrDefault();
            var cooling = userDetailsResponse.Select(x => x.Cooling ?? null).FirstOrDefault();
            var mainmotortemperaturerise = userDetailsResponse.Select(x => x.Main_motor_temperature_rise ?? null).FirstOrDefault();

            #region "Report Paramaterwise PDF"
            if ((code == "EMH21-44"  || code == "TMC6-26A" || code == "TMC54-85") && ((Convert.ToDouble(Capacity_at_normal_working_pressure_m_min) > 0 && mainmotortemperaturerise !=null)))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu", new { id = id });
            }
            else if (code != "TMC400-450" && Convert.ToDouble(Main_motor_F_class_IP_55_kW) > 87 && cooling == "EA")
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu", new { id = id });
            }
            else if (code == "WD7-22A" || code == "WD21-44" || code == "WD54-85")
            {

                return new Rotativa.MVC.ActionAsPdf("Datasivu_WD", new { id = id });
            }
            else if (code == "EMH21-44"  && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_UUSI", new { id = id });
            }
            else if (code == "EMH21-44" && Capacity_at_normal_working_pressure_m_min is null && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_IE3", new { id = id });
            }
            else if (code == "TMC7-27" && model != "TMC 21 SA" && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC727datasivu", new { id = id });
            }
            else if (code == "TMC7-27" && model != "TMC 21 SA" && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC7_27datasivu_IE3", new { id = id });
            }
            else if (code == "TMC54-85"  && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC54_85datasivu", new { id = id });
            }
            else if (code == "TMC54-85"  && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC54_85datasivu_IE3", new { id = id });
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_SA", new { id = id });
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_SA_UUSI", new { id = id });
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_SA_IE3", new { id = id });
            }
            else if (code == "TMC7-27" && model == "TMC 21 SA" && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC21SAdatasivu", new { id = id });
            }
            else if (code == "TMC7-27" && model == "TMC 21 SA" && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("TMC21SAdatasivu_IE3", new { id = id });
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200") && cooling == "EW" && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot", new { id = id });
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200") && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_UUSI", new { id = id });
            }
            else if ((code == "TMC245-360" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_UUSI", new { id = id });
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3", new { id = id });
            }
            else if ((code == "TMC245-360" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3", new { id = id });
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && cooling == "EA / EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3_EAEW", new { id = id });
            }

            else if ((code == "TMC245-360" && (cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3_EAEW", new { id = id });
            }
            else if ((code == "TMC150-235" && (cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3_EAEW", new { id = id });
            }
            else if ((code == "TMC150-235" && (cooling == "EA / EW v2") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3_EAEW", new { id = id });
            }
            else if (code == "TMC400-450" && cooling == "EA")
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_IE3_EAEW", new { id = id });
            }
            else if (cooling == "HW")
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_HYD", new { id = id });
            }
            else if ((code == "TMC105-235" || code == "TMC150-235SA" || code == "TMC240-365SA") && cooling == "EW" && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA", new { id = id });
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA") && cooling == "EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA_UUSI", new { id = id });
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA_IE3", new { id = id });
            }
            else if ((code == "TMC245-360SA" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA_IE3", new { id = id });
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EA / EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA_IE3_EAEW", new { id = id });
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EA / EW v2" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_SA_IE3_EAEW", new { id = id });
            }
            else if (code == "TMC86-124" && model != "TMC 95 SA")
            {
                return new Rotativa.MVC.ActionAsPdf("TMC86_124datasivu", new { id = id });
            }
            else if (code == "WD7-22B")
            {
                return new Rotativa.MVC.ActionAsPdf("WD722Bdatasivu", new { id = id });
            }
            else if (code == "RRM105-185" && mainmotortemperaturerise is null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_RRM", new { id = id });
            }
            else if (code == "RRM105-185" && mainmotortemperaturerise != null)
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_isot_RRM_IE3", new { id = id });
            }
            else if (code == "TMC86-124" && model == "TMC 95 SA")
            {
                return new Rotativa.MVC.ActionAsPdf("TMC95SAdatasivu", new { id = id });
            }
            else if (code == "TMC49-84" && (model == "TMC 49" || model == "TMC 64" || model == "TMC 84"))
            {
                return new Rotativa.MVC.ActionAsPdf("TMC49_84datasivu", new { id = id });
            }
            else if (code == "TMC49-84" && (model == "TMC 55 SA" || model == "TMC 84 SA"))
            {
                return new Rotativa.MVC.ActionAsPdf("TMC55_84SAdatasivu", new { id = id });
            }
            else if (code == "TMC35-44" && (model == "TMC 35" || model == "TMC 44"))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_IE3", new { id = id });
            }
            else if (code == "TMC35-44" && (model == "TMC 35 SA" || model == "TMC 44 SA"))
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_SA_IE3", new { id = id });
            }
            else
            {
                return new Rotativa.MVC.ActionAsPdf("Datasivu_IE3", new { id = id });
            }
            #endregion
        }
        #region"Word/Docx Paramater"
        public string GetPrinTDocType(int id)
        {
            MarineDataEntities dc = new MarineDataEntities();
            var userDetailsResponse = dc.Data.Where(x => x.Laskuri == id);
            var code = userDetailsResponse.Select(x => x.Code ?? "").FirstOrDefault().ToString();
            var Capacity_at_normal_working_pressure_m_min = userDetailsResponse.Select(x => x.Capacity_at_normal_working_pressure_m_min ?? null).FirstOrDefault();
            var Extra_factor_for_water_flow_at_60_humidity_fresh = userDetailsResponse.Select(x => x.Extra_factor_for_water_flow_at_60_humidity_fresh ?? null).FirstOrDefault();
            var Main_motor_F_class_IP_55_kW = userDetailsResponse.Select(x => x.Main_motor_F_class_IP_55_kW).FirstOrDefault();
            var model = userDetailsResponse.Select(x => x.Model_No ?? null).FirstOrDefault();
            var cooling = userDetailsResponse.Select(x => x.Cooling ?? null).FirstOrDefault();
            var mainmotortemperaturerise = userDetailsResponse.Select(x => x.Main_motor_temperature_rise ?? null).FirstOrDefault();

            #region "Report Paramaterwise PDF"
            if ((code == "EMH21-44" ||  code == "TMC6-26A" || code == "TMC54-85") && (Convert.ToDouble(Capacity_at_normal_working_pressure_m_min) > 0) && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu", Model, true);
                return str;
               
            }
            else if (code != "TMC400-450" && Convert.ToDouble(Main_motor_F_class_IP_55_kW) > 87 && cooling == "EA")
            {
                var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu", Model, true);
                return str;
                
            }
            else if (code == "WD7-22A" || code == "WD21-44" || code == "WD54-85")
            {
                var Model = md.RPT_GetReportData_Datasivu_WD.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_WD", Model, true);
                return str;
                
            }
            else if (code == "EMH21-44"  && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_Datasivu_UUSI.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_UUSI", Model, true);
                return str;
               
            }
            else if (code == "EMH21-44" && Capacity_at_normal_working_pressure_m_min is null && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_IE3", Model, true);
                return str;
                
            }
            else if (code == "TMC7-27" && model != "TMC 21 SA" && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_TMC_7_27_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_7_27_datasivu", Model, true);
                return str;
               ;
            }
            else if (code == "TMC7-27" && model != "TMC 21 SA" && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_TMC_7_27_datasivu_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_7_27_datasivu_IE3", Model, true);
                return str;
               
            }
            else if (code == "TMC54-85" && Capacity_at_normal_working_pressure_m_min != null && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_TMC54_85_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC54_85_datasivu", Model, true);
                return str;
               
            }
            else if (code == "TMC54-85" && Capacity_at_normal_working_pressure_m_min != null && mainmotortemperaturerise !=null )
            {
                var Model = md.RPT_GetReportData_TMC54_85_datasivu_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC54_85_datasivu_IE3", Model, true);
                return str;
                
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                var Model = md.RPT_GetReportData_Datasivu_SA.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_SA", Model, true);
                return str;
               
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_Datasivu_SA_UUSI.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_SA_UUSI", Model, true);
                return str;
               
            }
            else if ((code == "TMC20-40SA" || code == "TMC30-40SA" || code == "TMC60-80SA" || code == "VS40" || code == "VS60-80") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_Datasivu_SA_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_SA_IE3", Model, true);
                return str;
                
            }
            else if (code == "TMC7-27" && model == "TMC 21 SA" && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_TMC_21_SA_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_21_SA_datasivu", Model, true);
                return str;
               
            }
            else if (code == "TMC7-27" && model == "TMC 21 SA" && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_TMC_21_SA_datasivu_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_21 _SA_datasivu_IE3", Model, true);
                return str;
                
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200") && cooling == "EW" && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                var Model = md.RPT_GetReportData_isot.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200") && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_isot_UUSI.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_UUSI", Model, true);
                return str;
               
            }
            else if ((code == "TMC245-360" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null))
            {
                var Model = md.RPT_GetReportData_isot_SA_UUSI.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_UUSI", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3", Model, true);
                return str;
               
            }
            else if ((code == "TMC245-360" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                var Model = md.RPT_GetReportData_isot_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3", Model, true);
                return str;
                
            }
            else if ((code == "TMC105-235" || code == "TMC105-235SL" || code == "TMC240-365" || code == "ULM90-200" || code == "TMC400-450") && cooling == "EA / EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3_EAEW", Model, true);
                return str;
               
            }

            else if ((code == "TMC245-360" && (cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3_EAEW", Model, true);
                return str;
               
            }
            else if ((code == "TMC150-235" && (cooling == "EA / EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3_EAEW", Model, true);
                return str;
                
            }
            else if ((code == "TMC150-235" && (cooling == "EA / EW v2") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3_EAEW", Model, true);
                return str;
                
            }
            else if (code == "TMC400-450" && cooling == "EA")
            {
                var Model = md.RPT_GetReportData_isot_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_IE3_EAEW", Model, true);
                return str;
               
            }
            else if (cooling == "HW")
            {
                var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_HYD", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235" || code == "TMC150-235SA" || code == "TMC240-365SA") && cooling == "EW" && Extra_factor_for_water_flow_at_60_humidity_fresh is null)
            {
                var Model = md.RPT_GetReportData_isot_SA.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA") && cooling == "EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_isot_SA_UUSI.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA_UUSI", Model, true);
                return str;
                
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_SA_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA_IE3", Model, true);
                return str;
                
            }
            else if ((code == "TMC245-360SA" && (cooling == "EW") && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null))
            {
                var Model = md.RPT_GetReportData_isot_SA_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA_IE3", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EA / EW" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_SA_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA_IE3_EAEW", Model, true);
                return str;
               
            }
            else if ((code == "TMC105-235SA" || code == "TMC150-235SA" || code == "TMC240-365SA" || code == "TMC400-450SA") && cooling == "EA / EW v2" && Convert.ToDecimal(Extra_factor_for_water_flow_at_60_humidity_fresh) > 0 && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_SA_IE3_EAEW.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_SA_IE3_EAEW", Model, true);
                return str;
                
            }
            else if (code == "TMC86-124" && model != "TMC 95 SA")
            {
                var Model = md.RPT_GetReportData_TMC_86_124_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_86_124_datasivu", Model, true);
                return str;
               
            }
            else if (code == "WD7-22B")
            {
                var Model = md.RPT_GetReportData_WD7_22B_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportWD7_22B_datasivu", Model, true);
                return str;
               
            }
            else if (code == "RRM105-185" && mainmotortemperaturerise is null)
            {
                var Model = md.RPT_GetReportData_isot_RRM.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_RRM", Model, true);
                return str;
               
            }
            else if (code == "RRM105-185" && mainmotortemperaturerise != null)
            {
                var Model = md.RPT_GetReportData_isot_RRM_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_isot_RRM_IE3", Model, true);
                return str;
                
            }
            else if (code == "TMC86-124" && model == "TMC 95 SA")
            {
                var Model = md.RPT_GetReportData_TMC_95_SA_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_95_SA_datasivu", Model, true);
                return str;
               
            }
            else if (code == "TMC49-84" && (model == "TMC 49" || model == "TMC 64" || model == "TMC 84"))
            {
                var Model = md.RPT_GetReportData_TMC_49_84_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_49_84_datasivu", Model, true);
                return str;
               
            }
            else if (code == "TMC49-84" && (model == "TMC 55 SA" || model == "TMC 84 SA"))
            {
                var Model = md.RPT_GetReportData_TMC_55_84_SA_datasivu.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportTMC_55_84_SA _datasivu", Model, true);
                return str;
                
            }
            else if (code == "TMC35-44" && (model == "TMC 35" || model == "TMC 44"))
            {
                var Model = md.RPT_GetReportData_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_IE3", Model, true);
                return str;
               
            }
            else if (code == "TMC35-44" && (model == "TMC 35 SA" || model == "TMC 44 SA"))
            {
                var Model = md.RPT_GetReportData_Datasivu_SA_IE3.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_SA_IE3", Model, true);
                return str;
               
            }
            else
            {
                var Model = md.RPT_GetReportData.Where(m => m.Laskuri == id);
                string str = MvcHelpers.RenderViewToString(this.ControllerContext, "ExportDatasivu_IE3", Model);
                return str;
            }
            #endregion


        }

       

        #endregion
    }
}