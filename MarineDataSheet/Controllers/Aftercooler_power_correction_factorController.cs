﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MarineDataSheet;

namespace MarineDataSheet.Controllers
{
    public class Aftercooler_power_correction_factorController : Controller
    {
        private MarineDataEntities db = new MarineDataEntities();

        // GET: Aftercooler_power_correction_factor
        public ActionResult Index()
        {
            return View(db.Aftercooler_power_correction_factors.ToList());
        }

        // GET: Aftercooler_power_correction_factor/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aftercooler_power_correction_factor aftercooler_power_correction_factor = db.Aftercooler_power_correction_factors.Find(id);
            if (aftercooler_power_correction_factor == null)
            {
                return HttpNotFound();
            }
            return View(aftercooler_power_correction_factor);
        }

        // GET: Aftercooler_power_correction_factor/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Aftercooler_power_correction_factor/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,powercorrection,Weighting")] Aftercooler_power_correction_factor aftercooler_power_correction_factor)
        {
            if (ModelState.IsValid)
            {
                db.Aftercooler_power_correction_factors.Add(aftercooler_power_correction_factor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aftercooler_power_correction_factor);
        }

        // GET: Aftercooler_power_correction_factor/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aftercooler_power_correction_factor aftercooler_power_correction_factor = db.Aftercooler_power_correction_factors.Find(id);
            if (aftercooler_power_correction_factor == null)
            {
                return HttpNotFound();
            }
            return View(aftercooler_power_correction_factor);
        }

        // POST: Aftercooler_power_correction_factor/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,powercorrection,Weighting")] Aftercooler_power_correction_factor aftercooler_power_correction_factor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aftercooler_power_correction_factor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aftercooler_power_correction_factor);
        }

        // GET: Aftercooler_power_correction_factor/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Aftercooler_power_correction_factor aftercooler_power_correction_factor = db.Aftercooler_power_correction_factors.Find(id);
            if (aftercooler_power_correction_factor == null)
            {
                return HttpNotFound();
            }
            return View(aftercooler_power_correction_factor);
        }

        // POST: Aftercooler_power_correction_factor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Aftercooler_power_correction_factor aftercooler_power_correction_factor = db.Aftercooler_power_correction_factors.Find(id);
            db.Aftercooler_power_correction_factors.Remove(aftercooler_power_correction_factor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
