﻿using MarineDataSheet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Data.Entity;

namespace MarineDataSheet.Controllers
{
    public class AllRolesController : Controller
    {
        MarineDataEntities md = new MarineDataEntities();
        // GET: AllRoles
        public ActionResult Roles()
        {
            return View();
        }
        public ActionResult LoadRolesData()
        {
            try
            {
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                int recordsTotal = 0;

                var rolesData = ShowallRoles(sortColumn, sortColumnDir, searchValue);
                recordsTotal = rolesData.Count();
                var data = rolesData.Skip(skip).Take(pageSize).ToList();

                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IQueryable<UserModel> ShowallRoles(string sortColumn, string sortColumnDir, string Search)
        {
            var _context = new MarineDataEntities();

            var IQueryabletimesheet = (from AssignedRoles in _context.AssignedRoles
                                       join registration in _context.Registrations on AssignedRoles.RegistrationID equals registration.RegistrationID
                                       join AssignedRolesAdmin in _context.Registrations on AssignedRoles.AssignToAdmin equals AssignedRolesAdmin.RegistrationID
                                       select new UserModel
                                       {
                                           Name = registration.Name,
                                           AssignToAdmin = string.IsNullOrEmpty(AssignedRolesAdmin.Name) ? "*Not Assigned*" : AssignedRolesAdmin.Name.ToUpper(),
                                           RegistrationID = registration.RegistrationID

                                       });

            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                IQueryabletimesheet = IQueryabletimesheet.OrderBy(sortColumn + " " + sortColumnDir);
            }
            if (!string.IsNullOrEmpty(Search))
            {
                IQueryabletimesheet = IQueryabletimesheet.Where(m => m.Name == Search);
            }

            return IQueryabletimesheet;

        }

        public ActionResult RemovefromRole(int RegistrationID)
        {
            var _context = new MarineDataEntities();
            try
            {
                if (RegistrationID ==0)
                {
                    return RedirectToAction("Roles");
                }
               var role= md.AssignedRoles.Where(x => x.RegistrationID == RegistrationID).First();
                md.AssignedRoles.Remove(role);
                md.SaveChanges();
                return Json(role);
            }
            catch (Exception)
            {
                return Json(false);
            }
            
        }
    }
}