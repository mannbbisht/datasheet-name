﻿using EventApplicationCore.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarineDataSheet.Controllers
{
    public class RegistrationController : Controller
    {
        MarineDataEntities md = new MarineDataEntities();
        // GET: Registration
        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(Registration registration)
        {
            Registration NewUserRegister = new Registration();
            registration.CreatedOn = DateTime.Now;
            if (registration.Password == null || registration.ConfirmPassword == null || registration.Username == null)
            {
                TempData["MessageErrorRegistration"] = "Plese enter the Username and Password";
                return View("Registration");
            }
            NewUserRegister.Password = EncryptionLibrary.EncryptText(registration.Password);
            NewUserRegister.ConfirmPassword = EncryptionLibrary.EncryptText(registration.ConfirmPassword);
            NewUserRegister.Name = registration.Name;
            NewUserRegister.Mobileno = registration.Mobileno;
            NewUserRegister.EmailID = registration.EmailID;
            NewUserRegister.Gender = registration.Gender;
            NewUserRegister.Birthdate = registration.Birthdate;
            NewUserRegister.DateofJoining = registration.DateofJoining;
            NewUserRegister.Username = registration.Username;
            NewUserRegister.Username = registration.Username;
            NewUserRegister.RoleID = 2; // FOR User
            md.Registrations.Add(NewUserRegister);
            md.SaveChanges();
            
                TempData["MessageRegistration"] = "Data Saved Successfully!";
                
                return View(registration);
            }
        }


}

