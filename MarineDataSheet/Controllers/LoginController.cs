﻿using EventApplicationCore.Library;
using MarineDataSheet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MarineDataSheet.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        MarineDataEntities md = new MarineDataEntities();
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            try
            {
                

                if (!string.IsNullOrEmpty(loginViewModel.Username) && !string.IsNullOrEmpty(loginViewModel.Password))
                {
                    var Username = loginViewModel.Username;
                    var password = EncryptionLibrary.EncryptText(loginViewModel.Password);

                    var result = ValidateUser(Username, password);

                    if (result != null)
                    {
                        if (result.RegistrationID == 0 || result.RegistrationID < 0)
                        {
                            ViewBag.errormessage = "Entered Invalid Username and Password";
                        }
                        else
                        {
                            var RoleID = result.RoleID;
                           //Remove Anonymous_Cookies

                            Session["RoleID"] = Convert.ToString(result.RoleID);
                            Session["Username"] = Convert.ToString(result.Username);
                            if (RoleID == 1)
                            {
                                Session["AdminUser"] = Convert.ToString(result.RegistrationID);

                                if (result.ForceChangePassword == 1)
                                {
                                    return RedirectToAction("ChangePassword", "UserProfile");
                                }

                                //return RedirectToAction("Dashboard", "Admin");
                                return RedirectToAction("Dashboard", "ViewDashboard");
                            }
                            else if (RoleID == 2)
                            {
                                //if (!_IAssignRoles.CheckIsUserAssignedRole(result.RegistrationID))
                                //{
                                //    ViewBag.errormessage = "Approval Pending";
                                //    return View(loginViewModel);
                                //}

                                Session["UserID"] = Convert.ToString(result.RegistrationID);

                                if (result.ForceChangePassword == 1)
                                {
                                    return RedirectToAction("ChangePassword", "UserProfile");
                                }

                                //return RedirectToAction("Dashboard", "User");
                                return RedirectToAction("Dashboard", "ViewDashboard");
                            }
                            else if (RoleID == 3)
                            {
                                Session["SuperAdmin"] = Convert.ToString(result.RegistrationID);
                                return RedirectToAction("Dashboard", "ViewDashboard");
                            }
                        }
                    }
                    else
                    {
                        ViewBag.errormessage = "Entered Invalid Username and Password";
                        return View(loginViewModel);
                    }
                }
                return View(loginViewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Registration ValidateUser(string userName, string passWord)
        {
            try
            {
                using (var _context = new MarineDataEntities())
                {
                    var validate = (from user in _context.Registrations
                                    where user.Username == userName && user.Password == passWord
                                    select user).FirstOrDefault();

                    return validate;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        [HttpGet]
        public ActionResult Logout()
        {

            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["SuperAdmin"])))
                {
                  
                }

                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                Response.Cache.SetNoStore();

                HttpCookie Cookies = new HttpCookie("WebTime");
                Cookies.Value = "";
                Cookies.Expires = DateTime.Now.AddHours(-1);
                Response.Cookies.Add(Cookies);
                HttpContext.Session.Clear();
                Session.Abandon();
                return RedirectToAction("Login", "Login");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [NonAction]
        public void remove_Anonymous_Cookies()
        {
            try
            {

                if (Request.Cookies["WebTime"] != null)
                {
                    var option = new HttpCookie("WebTime");
                    option.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(option);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}