﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;


namespace MarineDataSheet.Controllers
{
    public class NewDataSheetController : Controller
    {
        // GET: NewDataSheet
        MarineDataEntities dbobje = new MarineDataEntities();
        
        private static log4net.ILog Log { get; set; }
        ILog log = log4net.LogManager.GetLogger(typeof(NewDataSheetController));
        public ActionResult Index()
        {
            var LatestDatasheetNo = dbobje.Usp_GetLatesDatasheetNumber().SingleOrDefault();
            ViewBag.Message = LatestDatasheetNo;
            return View();
        }

        [HttpPost]
        public ActionResult Index(string button,Datum model)
        {
            
            if (button == "Clear")  //I can see code is executed here, I need to clear all UI fields starts from there.
            {
                //here, I need to clear all Input text fields, text, listbox, dropdownlist, etc.   
                ModelState.Clear();
                return View(); //this will return the original UI razor view,

            }
            else {

                try
                {
                    if (model.Datasivun_numero == null || model.Valid_from == null || model.Datasheet_status == null)
                    {
                        TempData["MessageErrorNewDatasheet"] = "Plese enter the Valid Detail";
                        return View("Index");
                    }
                    else {
                        var results = dbobje.Data
                        .Where(b => b.Datasivun_numero.Equals(model.Datasivun_numero))
                        .FirstOrDefault();

                        if(results==null)
                        {
                            Datum tblobj = new Datum();
                            tblobj.STD = model.STD;
                            tblobj.Datasivun_numero = model.Datasivun_numero;
                            tblobj.Datasheet_status = model.Datasheet_status;
                            tblobj.Code = model.Code;
                            tblobj.Datasheet_created_by = model.Datasheet_created_by;
                            tblobj.Model_No = model.Model_No;
                            tblobj.Maximum_working_pressure_barg = model.Maximum_working_pressure_barg;
                            tblobj.Cooling = model.Cooling;
                            tblobj.Valid_from = model.Valid_from;
                            tblobj.C1_Voltage = model.C1_Voltage;
                            tblobj.C1_Frequency = model.C1_Frequency;
                            tblobj.C1_Starting_type = model.C1_Starting_type;
                            tblobj.C2_Voltage = model.C2_Voltage;
                            tblobj.C2_Frequency = model.C2_Frequency;
                            tblobj.C2_Starting_type = model.C2_Starting_type;
                            tblobj.Normal_working_pressure_barg = model.Normal_working_pressure_barg;
                            tblobj.Capacity_at_normal_working_pressure_m_min = model.Capacity_at_normal_working_pressure_m_min;
                            tblobj.Capacity_at_normal_working_pressure_m_min_75 = model.Capacity_at_normal_working_pressure_m_min_75;
                            tblobj.Capacity_at_normal_working_pressure_m_min_50 = model.Capacity_at_normal_working_pressure_m_min_50;
                            tblobj.Capacity_at_normal_working_pressure_m_min_25 = model.Capacity_at_normal_working_pressure_m_min_25;
                            tblobj.Capacity_at_normal_working_pressure_m_min_min = model.Capacity_at_normal_working_pressure_m_min_min;
                            tblobj.Volumetric_efficiency = model.Volumetric_efficiency;
                            tblobj.Air_end_volume_per_revolution = model.Air_end_volume_per_revolution;
                            tblobj.Shaft_power_at_normal_working_pressure_kW = model.Shaft_power_at_normal_working_pressure_kW;
                            tblobj.Shaft_power_at_normal_working_pressure_kW_75 = model.Shaft_power_at_normal_working_pressure_kW_75;
                            tblobj.Shaft_power_at_normal_working_pressure_kW_50 = model.Shaft_power_at_normal_working_pressure_kW_50;
                            tblobj.Shaft_power_at_normal_working_pressure_kW_25 = model.Shaft_power_at_normal_working_pressure_kW_25;
                            tblobj.Shaft_power_at_normal_working_pressure_kW_min = model.Shaft_power_at_normal_working_pressure_kW_min;
                            tblobj.Specific_power_consumption_kW_m_min = model.Specific_power_consumption_kW_m_min;
                            tblobj.Extra_capacity = model.Extra_capacity;
                            tblobj.Minimum_working_pressure_barg = model.Minimum_working_pressure_barg;
                            tblobj.Frequency_min = model.Frequency_min;
                            tblobj.Frequency_max = model.Frequency_max;
                            tblobj.Transmission = model.Transmission;
                            tblobj.Transmission_Gear_i = model.Transmission_Gear_i;
                            tblobj.Motor_pulley = model.Motor_pulley;
                            tblobj.Air_end_pulley = model.Air_end_pulley;
                            tblobj.Maximum_ambient_temperature_C = model.Maximum_ambient_temperature_C;
                            tblobj.Compressed_air_temperature_above_cooling_medium_temperature_C = model.Compressed_air_temperature_above_cooling_medium_temperature_C;
                            tblobj.Cooling_air_flow_m_s = model.Cooling_air_flow_m_s;
                            tblobj.Cooling_air_flow_m_s_water_cooled = model.Cooling_air_flow_m_s_water_cooled;
                            tblobj.Maximum_cooling_air_pressure_drop_Pa = model.Maximum_cooling_air_pressure_drop_Pa;
                            tblobj.Water_in_C_Fresh_water_cooled = model.Water_in_C_Fresh_water_cooled;
                            tblobj.Water_out_C_Fresh_water_cooled = model.Water_out_C_Fresh_water_cooled;
                            tblobj.Water_in_C_Sea_water_cooled = model.Water_in_C_Sea_water_cooled;
                            tblobj.Water_out_C_Sea_water_cooled = model.Water_out_C_Sea_water_cooled;
                            tblobj.Extra_factor_for_water_flow_at_60_humidity_fresh = model.Extra_factor_for_water_flow_at_60_humidity_fresh;
                            tblobj.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial = model.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Serial;
                            tblobj.Minimum_water_inlet_pressure_with_zero_ba_bar_MPa_Parallel = model.Minimum_water_inlet_pressure_with_zero_ba_bar_MPa_Parallel;
                            tblobj.Minimum_water_inlet_pressure_with_zero_back_Serial_sea = model.Minimum_water_inlet_pressure_with_zero_back_Serial_sea;
                            tblobj.Minimum_water_inlet_pressure_with_zero_back_Parallel_sea = model.Minimum_water_inlet_pressure_with_zero_back_Parallel_sea;
                            tblobj.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Oil = model.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_Oil;
                            tblobj.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_After = model.Minimum_water_inlet_pressure_with_zero_back_bar_MPa_After;
                            tblobj.Maximum_inlet_pressure_barg = model.Maximum_inlet_pressure_barg;
                            tblobj.Water_in_out_T2_T3 = model.Water_in_out_T2_T3;
                            tblobj.Heat_dissipation_kW_EA = model.Heat_dissipation_kW_EA;
                            tblobj.Heat_dissipation_kW_EW = model.Heat_dissipation_kW_EW;
                            tblobj.Main_motor_F_class_IP_55_kW = model.Main_motor_F_class_IP_55_kW;
                            tblobj.Main_motor_protection_details = model.Main_motor_protection_details;
                            tblobj.Main_motor_temperature_rise = model.Main_motor_temperature_rise;
                            tblobj.Speed_of_rotation_rpm = model.Speed_of_rotation_rpm;
                            tblobj.Speed_of_rotation_rpm_min = model.Speed_of_rotation_rpm_min;
                            tblobj.Motor_cos_fii = model.Motor_cos_fii;
                            tblobj.Motor_efficiency = model.Motor_efficiency;
                            tblobj.Fan_motor_kW_EA = model.Fan_motor_kW_EA;
                            tblobj.Fan_motor_kW_EW = model.Fan_motor_kW_EW;
                            tblobj.Speed_of_rotation_rpm_fan_motor = model.Speed_of_rotation_rpm_fan_motor;
                            tblobj.Voltage_tolerance = model.Voltage_tolerance;
                            tblobj.Hydraulic_main_motor = model.Hydraulic_main_motor;
                            tblobj.Hydraulic_motor_speed_of_rotation_Max = model.Hydraulic_motor_speed_of_rotation_Max;
                            tblobj.Hydraulic_motor_speed_of_rotation_Min = model.Hydraulic_motor_speed_of_rotation_Min;
                            tblobj.Hydraulic_oil_flow_l_min = model.Hydraulic_oil_flow_l_min;
                            tblobj.Hydraulic_oil_flow_l_min_min = model.Hydraulic_oil_flow_l_min_min;
                            tblobj.Hydraulic_oil_pressure_bar = model.Hydraulic_oil_pressure_bar;
                            tblobj.Hydraulic_oil_pressure_bar_min = model.Hydraulic_oil_pressure_bar_min;
                            tblobj.Fuse_max_1_Jännite_A = model.Fuse_max_1_Jännite_A;
                            tblobj.Fuse_max_2_Jännite_A = model.Fuse_max_2_Jännite_A;
                            tblobj.Starting_current_Ia_In_DOL_YD_SOFT_START = model.Starting_current_Ia_In_DOL_YD_SOFT_START;
                            tblobj.Control_voltage_V = model.Control_voltage_V;
                            tblobj.Oil_quantity_I = model.Oil_quantity_I;
                            tblobj.Oil_quantity_EA_I = model.Oil_quantity_EA_I;
                            tblobj.Oil_content_mg_m = model.Oil_content_mg_m;
                            tblobj.Air_outlet_T1 = model.Air_outlet_T1;
                            tblobj.Air_outlet_T1_water_cooled = model.Air_outlet_T1_water_cooled;
                            tblobj.Condensate_drain_T4 = model.Condensate_drain_T4;
                            tblobj.Pressure_Level_LpA_without_canopy_EW = model.Pressure_Level_LpA_without_canopy_EW;
                            tblobj.Pressure_Level_LpA_without_canopy_EW = model.Pressure_Level_LpA_without_canopy_EW;
                            tblobj.Pressure_Level_LpA_with_NOVOX_canopy_EA = model.Pressure_Level_LpA_with_NOVOX_canopy_EA;
                            tblobj.Pressure_Level_LpA_with_NOVOX_canopy_EW = model.Pressure_Level_LpA_with_NOVOX_canopy_EW;
                            tblobj.Uncertainty_Kpa_Kwa = model.Uncertainty_Kpa_Kwa;
                            tblobj.Weight_without_canopy = model.Weight_without_canopy;
                            tblobj.Weight_with_NOVOX_canopy = model.Weight_with_NOVOX_canopy;
                            tblobj.Weight_with_NOVOX_canopy_EA = model.Weight_with_NOVOX_canopy_EA;
                            tblobj.General_arrangement_drawing_SW = model.General_arrangement_drawing_SW;
                            tblobj.General_arrangement_drawing_FW = model.General_arrangement_drawing_FW;
                            tblobj.General_arrangement_drawing_EA = model.General_arrangement_drawing_EA;
                            tblobj.General_arrangement_drawing_with_NOVOX_canopy_EA = model.General_arrangement_drawing_with_NOVOX_canopy_EA;
                            tblobj.General_arrangement_drawing_with_NOVOX_canopy_EW = model.General_arrangement_drawing_with_NOVOX_canopy_EW;
                            tblobj.General_arrangement_drawing_with_NOVOX_canopy_EW_SW = model.General_arrangement_drawing_with_NOVOX_canopy_EW_SW;
                            tblobj.Cable_gland_Power = model.Cable_gland_Power;
                            tblobj.Cable_gland_Alarm_Signal = model.Cable_gland_Alarm_Signal;
                            tblobj.Lisätietoja = model.Lisätietoja;
                            tblobj.Suunnittelun_kommentit = model.Suunnittelun_kommentit;
                            dbobje.Data.Add(tblobj);
                            dbobje.SaveChanges();
                            TempData["MessageCreateNewDataSheet"] = "Data Saved Successfully!";

                        }  
                        else
                        {
                            TempData["MessageErrorNewDatasheet"] = "DataSheet Already Exit!";
                            return View("Index");

                        }

                    }

                    
                }
                catch (DbEntityValidationException ex)
                {
                    TempData["MessageCreateNewDataSheet"] = "Error while Saving!";
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    log.Info(ex.StackTrace.ToString());
                    throw ex;
                }
            }
           

            return View();
        }
       
       
    }
}