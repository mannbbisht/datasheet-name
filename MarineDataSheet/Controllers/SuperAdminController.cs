﻿using EventApplicationCore.Library;
using MarineDataSheet.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace MarineDataSheet.Controllers
{
    public class SuperAdminController : Controller
    {
        MarineDataEntities md = new MarineDataEntities();

        [HttpGet]
        public ActionResult AssignRoles()
        {
            try
            {
                AssignRolesModel assignRolesModel = new AssignRolesModel();


                List<AdminModel> lstadmin = (from student in md.Registrations
                                             where student.RoleID == 1
                                             select new AdminModel
                                             {
                                                 RegistrationID = student.RegistrationID,
                                                 Name = student.Name
                                             }).ToList();




                List<UserModel> lstunassigned = (from student in md.Usp_GetListofUnAssignedUsers

                                                 select new UserModel
                                                 {
                                                     RegistrationID = student.RegistrationID,
                                                     Name = student.Name
                                                 }).ToList();






                assignRolesModel.ListofAdmins = lstadmin;
                assignRolesModel.ListofUser = lstunassigned;
                return View(assignRolesModel);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        public ActionResult AssignRoles(AssignRolesModel objassign)
        {

            try
            {
                if (objassign.ListofUser == null)
                {
                    //TempData["MessageErrorRoles"] = "There are no Users to Assign Roles";
                    //objassign.ListofAdmins = _IAssignRoles.ListofAdmins();
                    //objassign.ListofUser = _IAssignRoles.GetListofUnAssignedUsers();
                    return View(objassign);
                }


                var SelectedCount = (from User in objassign.ListofUser
                                     where User.selectedUsers == true
                                     select User).Count();
                var RegistationId = (from User in objassign.ListofUser
                                     where User.selectedUsers == true
                                     select User.RegistrationID).ToList();

                //var Adminid = (from User in objassign.ListofAdmins
                //                     where User.selectedUsers == true == true
                //                     select User.RegistrationID).FirstOrDefault();

                if (SelectedCount == 0)
                {
                    TempData["MessageErrorRoles"] = "You have not Selected any User to Assign Roles";
                    //objassign.ListofAdmins = _IAssignRoles.ListofAdmins();
                    //objassign.ListofUser = _IAssignRoles.GetListofUnAssignedUsers();
                    return RedirectToAction("AssignRoles");
                }

                if (ModelState.IsValid)
                {
                    for (int i = 0; i < RegistationId.Count(); i++)
                    {
                        AssignedRole asg = new AssignedRole();
                        asg.CreatedBy = Convert.ToInt32(Session["SuperAdmin"]);
                        asg.AssignToAdmin = objassign.RegistrationID;
                        asg.Status = "A";
                        asg.CreatedOn = DateTime.Now;
                        asg.RegistrationID = RegistationId[i];
                        md.Entry(asg).State = EntityState.Added;
                        md.SaveChanges();
                        TempData["MessageRoles"] = "Roles Assigned Successfully!";
                    }

                }
                objassign = new AssignRolesModel();
                //objassign.ListofAdmins = _IAssignRoles.ListofAdmins();
                //objassign.ListofUser = _IAssignRoles.GetListofUnAssignedUsers();

                return RedirectToAction("AssignRoles");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CreateAdmin()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdmin(Registration registration)
        {
            Registration NewUserRegister = new Registration();
            registration.CreatedOn = DateTime.Now;
            if (registration.Password == null || registration.ConfirmPassword == null || registration.Username == null)
            {
                TempData["MessageErrorRegistration"] = "Plese enter the Username and Password";
                return View("CreateAdmin");
            }
            NewUserRegister.Password = EncryptionLibrary.EncryptText(registration.Password);
            NewUserRegister.ConfirmPassword = EncryptionLibrary.EncryptText(registration.ConfirmPassword);
            NewUserRegister.Name = registration.Name;
            NewUserRegister.Mobileno = registration.Mobileno;
            NewUserRegister.EmailID = registration.EmailID;
            NewUserRegister.Gender = registration.Gender;
            NewUserRegister.Birthdate = registration.Birthdate;
            NewUserRegister.DateofJoining = registration.DateofJoining;
            NewUserRegister.Username = registration.Username;
            NewUserRegister.Username = registration.Username;
            NewUserRegister.RoleID = 1; // FOR ADMIN
            md.Registrations.Add(NewUserRegister);
            md.SaveChanges();

            TempData["MessageRegistration"] = "Data Saved Successfully!";

            return View(registration);
        }
    }
}