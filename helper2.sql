select 
((((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2)/60)
+(((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*9))/(4.2*([Water out C Fresh water cooled]-[Water in C Fresh water cooled])) *convert(float,replace([Extra factor for water flow at 60 humidity fresh],',','')) as 'Oil_cooler_heat_rejection'
,((((60-[Compressed air temperature above cooling medium temperature C])*[Capacity at normal working pressure m min]*1.2)/60)) +((0.04*[Capacity at normal working pressure m min]*1.1)/60)*2260*9 as'After_cooler_heat_rejection_at_60_air_humidity_45_temp'
,(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*((100-[Motor efficiency])/100))+(([Shaft power at normal working pressure kW]*100/[Motor efficiency]))*0.02 as 'Heat_dissipation_EW'
,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [1 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package440'
 ,Ceiling(Round(((isnull([Fan motor kW EW],0) +[Shaft power at normal working pressure kW])
 *1000*1.07
 )/(SQRT(3)* [2 Voltage]*[Motor cos fii]*([Motor efficiency]/100)),2)) as 'Current for package640'
 ,((((([Shaft power at normal working pressure kW]*100/[Motor efficiency])) *((100-[Motor efficiency])/100)) +(([Shaft power at normal working pressure kW]*100/[Motor efficiency])*0.02) )/(1.25*10)) as 'Cooling_air_flow_for_ventilation_of_compressor_room'
FROM  dbo.Data 
    WHERE CODE IN ('RRM105-185') AND
	 [Main motor temperature rise] IS  NULL 
	  and [Datasivun numero]='MDATA1157'